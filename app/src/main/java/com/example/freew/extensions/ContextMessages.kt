package com.example.freew.extensions

import android.content.Context
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.example.freew.R

fun Context.showMessageDialog(@StringRes message: Int, action: (() -> Void)? = null) {
    MaterialDialog.Builder(this)
        .title(R.string.app_name)
        .content(message)
        .cancelable(false)
        .positiveText(R.string.ok_message)
        .onPositive { _, _ -> action?.invoke() }
}

fun Context.showMessageDialog(message: String, action: (() -> Void)? = null) {
    MaterialDialog.Builder(this)
        .title(R.string.app_name)
        .content(message)
        .cancelable(false)
        .positiveText(R.string.ok_message)
        .onPositive { _, _ -> action?.invoke() }
}

fun Context.showErrorDialog(error: Throwable, action: (() -> Void)? = null) {
    MaterialDialog.Builder(this)
        .title(R.string.app_name)
        .content(error.localizedMessage)
        .cancelable(false)
        .positiveText(R.string.ok_message)
        .onPositive { _, _ -> action?.invoke() }
}
