package com.example.freew.extensions

import com.example.freew.ui.base.LoaderShower
import io.reactivex.Observable

fun <T> Observable<T>.triggerLoadingIndicator(shower: LoaderShower): Observable<T> = this
    .doOnSubscribe { shower.showLoader() }
    .doFinally { shower.hideLoader() }