package com.example.freew.extensions

import android.view.View
import com.example.freew.ui.base.BaseFragment
import com.example.freew.ui.base.ErrorShower
import com.trello.rxlifecycle2.LifecycleProvider
import com.trello.rxlifecycle2.kotlin.bindToLifecycle
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject


fun <T> View.bindClicksWithErrors(errorShower: ErrorShower, provider: BaseFragment, action: Observable<T>, isEnabledByUser: Observable<Boolean>? = null): Observable<Observable<T>> {
    val executions = PublishSubject.create<Observable<T>>()
    val isExecuting = BehaviorSubject.createDefault<Boolean>(false)

    val isEnabled = if (isEnabledByUser != null) {
        val combineEnabled = BiFunction<Boolean, Boolean, Boolean> { isUserEnabled, isExec -> isUserEnabled && !isExec }
        Observable.combineLatest(isEnabledByUser.startWith(false), isExecuting, combineEnabled)
    } else {
        isExecuting.map { !it }
    }

   val disp = isEnabled.bindToLifecycle(provider).subscribe(::setEnabled)

    setOnClickListener {
        val replay = action.bindToLifecycle(provider)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError { errorShower.showError(it) }
            .doFinally { isExecuting.onNext(false) }
            .replay()
        executions.onNext(replay)
        isExecuting.onNext(true)
        replay.connect()
    }
    return executions.bindToLifecycle(provider)
}

fun <T> View.bindClicks(errorShower: ErrorShower, provider: BaseFragment, action: Observable<T>, isEnabledByUser: Observable<Boolean>? = null): Observable<T> {
    return bindClicksWithErrors(errorShower, provider, action, isEnabledByUser)
        .flatMap {
            it.onErrorResumeNext { _: Throwable ->
                Observable.empty()
            }
        }


}
