package com.example.freew.api;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface WebInterface {
    @GET("/api/profile")
    public Observable<BaseResponse> sendPhone();
}
