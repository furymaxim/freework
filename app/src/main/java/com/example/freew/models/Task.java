package com.example.freew.models;

public class Task {

    private Long id;
    private String title;
    private boolean doesTypeTwoExist;
    private boolean doesTypeThreeExist;
    //1 - new 2-for favorites 3-special 4-hot
    private int typeOne, typeTwo, typeThree;
    private String organizationName;
    private String address;
    private String metro;
    private String startDate, endDate;
    private String startTime, endTime;
    private String salary;
    private String actualDate;


    public Task(Long id, String actualDate,String title, boolean doesTypeTwoExist, boolean doesTypeThreeExist, int typeOne, int typeTwo, int typeThree, String organizationName, String address, String metro, String startDate, String endDate, String startTime, String endTime, String salary) {
        this.id = id;
        this.actualDate = actualDate;
        this.title = title;
        this.doesTypeTwoExist = doesTypeTwoExist;
        this.doesTypeThreeExist = doesTypeThreeExist;
        this.typeOne = typeOne;
        this.typeTwo = typeTwo;
        this.typeThree = typeThree;
        this.organizationName = organizationName;
        this.address = address;
        this.metro = metro;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.salary = salary;
    }

    public String getActualDate() {
        return actualDate;
    }

    public void setActualDate(String actualDate) {
        this.actualDate = actualDate;
    }

    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean doesTypeTwoExist() {
        return doesTypeTwoExist;
    }

    public void setDoesTypeTwoExist(boolean doesTypeTwoExist) {
        this.doesTypeTwoExist = doesTypeTwoExist;
    }

    public boolean doesTypeThreeExist() {
        return doesTypeThreeExist;
    }

    public void setDoesTypeThreeExist(boolean doesTypeThreeExist) {
        this.doesTypeThreeExist = doesTypeThreeExist;
    }


    public int getTypeOne() {
        return typeOne;
    }

    public void setTypeOne(int typeOne) {
        this.typeOne = typeOne;
    }

    public int getTypeTwo() {
        return typeTwo;
    }

    public void setTypeTwo(int typeTwo) {
        this.typeTwo = typeTwo;
    }

    public int getTypeThree() {
        return typeThree;
    }

    public void setTypeThree(int typeThree) {
        this.typeThree = typeThree;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMetro() {
        return metro;
    }

    public void setMetro(String metro) {
        this.metro = metro;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }


}
