package com.example.freew.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.freew.R;
import com.example.freew.models.SortOption;
import com.example.freew.ui.fragments.index.SortCompaniesFragment;

import java.util.ArrayList;

public class SortListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SortOption> items;

    public SortListAdapter(Context context, ArrayList<SortOption> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.sort_item,parent,false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
       else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SortOption currentItem = items.get(position);
        viewHolder.itemName.setText(currentItem.getTitle());

        if(currentItem.isSelected()){
            viewHolder.itemIsSelected.setBackgroundResource(R.drawable.ic_check_box_on);
        }else{
            viewHolder.itemIsSelected.setBackgroundResource(R.drawable.ic_check_box_off);
        }


       // viewHolder.itemIsSelected.setTag(position);

       // viewHolder.itemIsSelected.setOnCheckedChangeListener(this);


                   /*if (position == 0) {
                        for (int i = 1; i < items.size(); i++) {
                            SortOption currentItem = items.get(i);
                            currentItem.setTitle(items.get(i).getTitle());
                            currentItem.setSelected(false);
                            items.set(i,currentItem);
                            notifyDataSetChanged();
                        }
                        notifyDataSetChanged();
                    }*/

                  /* if(isChecked) {
                        SortOption firstItem = items.get(0);
                        firstItem.setTitle(items.get(0).getTitle());
                        firstItem.setSelected(false);
                        items.set(0,firstItem);
                        notifyDataSetChanged();

                    }*/





        /*viewHolder.itemIsSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                viewHolder.itemIsSelected.setChecked(isChecked);
                for(int i=0; i< items.size();i++){
                    if(currentItem.getSortByTitle().equals(items.get(i).getSortByTitle())){
                        continue;
                    }else{
                        items.get(i).setSelected(false);

                    }
                }
            }
        });

*/
        return convertView;
    }


/*    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //((SortOption)getItem((Integer)buttonView.getTag())).setSelected(isChecked);
        //if(isChecked){

        }
        Toast.makeText(context,String.valueOf(buttonView.getTag()),Toast.LENGTH_SHORT).show();

    }*/


    public void updateSortOptions(ArrayList<SortOption> sortOptions){
        this.items = sortOptions;

        notifyDataSetChanged();

    }


/*    public boolean isChecked(int position) {
        return items.get(position).isSelected();
    }

    public void setChecked(int position, boolean isChecked) {
        SortOption item = items.get(position);
        item.setSelected(isChecked);
        items.set(position, item);
        notifyDataSetChanged();

    }

    public void toggle(int position) {
        setChecked(position, !isChecked(position));

    }*/

  public ArrayList<SortOption> getSelectedItems(){

        ArrayList<SortOption> selectedItems = new ArrayList<>();

        for (SortOption p : items) {
            if (p.isSelected())
                selectedItems.add(p);
        }
        return selectedItems;

    }

    public void unselectedItems(){
        for (SortOption p : items) {
            if (p.isSelected())
                p.setSelected(false);
        }

        notifyDataSetChanged();

    }


    private class ViewHolder {
        TextView itemName;
        ImageView itemIsSelected;

        ViewHolder(View view) {
            itemName = view.findViewById(R.id.itemTitle);
            itemIsSelected = view.findViewById(R.id.checkbox);

        }

    }
}
