package com.example.freew.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.freew.R;
import com.example.freew.models.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> {

    private List<Task> taskList = new ArrayList<>();
    private OnTaskListener mOnTaskListener;

    public List<Task> getTaskList() {
        return taskList;
    }

    public TaskAdapter(OnTaskListener onTaskListener) {
        this.mOnTaskListener = onTaskListener;
    }

    public class TaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView title;
        private ImageView baseIcon,addOneImage,addTwoImage;
        private TextView organizationName, address, metro, startDate, endDate, startTime, endTime, salary;
        OnTaskListener onTaskListener;
        public RelativeLayout viewForeground, viewBackground;

        public TaskViewHolder(View itemView, OnTaskListener onTaskListener) {
            super(itemView);
            title = itemView.findViewById(R.id.itemTitle);
            baseIcon = itemView.findViewById(R.id.itemBaseIcon);
            addOneImage = itemView.findViewById(R.id.itemAddOneIcon);
            addTwoImage = itemView.findViewById(R.id.itemAddTwoIcon);
            organizationName = itemView.findViewById(R.id.itemOrganization);
            address = itemView.findViewById(R.id.itemAddress);
            metro = itemView.findViewById(R.id.itemMetro);
            startDate = itemView.findViewById(R.id.itemStartDate);
            endDate = itemView.findViewById(R.id.itemEndDate);
            startTime = itemView.findViewById(R.id.itemStartTime);
            endTime = itemView.findViewById(R.id.itemEndTime);
            salary = itemView.findViewById(R.id.itemSalary);
            this.onTaskListener = onTaskListener;
            viewForeground = itemView.findViewById(R.id.viewForeground);
            viewBackground = itemView.findViewById(R.id.viewBackground);




            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
                onTaskListener.onTaskClick(getAdapterPosition());
        }

        public void bind(Task task) {
            title.setText(task.getTitle());
            switch (task.getTypeOne()){
                case 1:
                    baseIcon.setImageResource(R.drawable.ic_task_new);
                    break;
                case 2:
                    baseIcon.setImageResource(R.drawable.ic_task_favorites);
                    break;
                case 3:
                    baseIcon.setImageResource(R.drawable.ic_task_special_offer);
                    break;
                case 4:
                    baseIcon.setImageResource(R.drawable.ic_task_hot);
                    break;
            }
            if(task.doesTypeTwoExist()){
                switch (task.getTypeTwo()){
                    case 1:
                        addOneImage.setImageResource(R.drawable.ic_task_new);
                        break;
                    case 2:
                        addOneImage.setImageResource(R.drawable.ic_task_favorites);
                        break;
                    case 3:
                        addOneImage.setImageResource(R.drawable.ic_task_special_offer);
                        break;
                    case 4:
                        addOneImage.setImageResource(R.drawable.ic_task_hot);
                        break;
                }
            }else{
                addOneImage.setVisibility(View.GONE);
            }
            if(task.doesTypeThreeExist()){
                switch (task.getTypeThree()){
                    case 1:
                        addTwoImage.setImageResource(R.drawable.ic_task_new);
                        break;
                    case 2:
                        addTwoImage.setImageResource(R.drawable.ic_task_favorites);
                        break;
                    case 3:
                        addTwoImage.setImageResource(R.drawable.ic_task_special_offer);
                        break;
                    case 4:
                        addTwoImage.setImageResource(R.drawable.ic_task_hot);
                        break;
                }
            }else{
                addTwoImage.setVisibility(View.GONE);
            }

            organizationName.setText(task.getOrganizationName());
            address.setText(task.getAddress());
            metro.setText(task.getMetro());
            startTime.setText(task.getStartTime());
            endTime.setText(task.getEndTime());
            startDate.setText(task.getStartDate());
            endDate.setText(task.getEndDate());
            salary.setText(task.getSalary());
        }
    }


    public void removeItem(int position){
            taskList.remove(position);
            notifyItemRemoved(position);
    }


    public void restoreItem(Task item, int position){
        taskList.add(position,item);
        notifyItemInserted(position);
    }

    public void setItems(Collection<Task> tasks) {
        taskList.addAll(tasks);
        notifyDataSetChanged();
    }

    public void clearItems() {
        taskList.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);

        return new TaskViewHolder(view, mOnTaskListener);

    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        holder.bind(taskList.get(position));
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    public interface OnTaskListener{
        void onTaskClick(int position);
    }

}
