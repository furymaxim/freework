package com.example.freew.helpers;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class CalendarRowHelper {




    public static String getCurrentMonth(){
        switch (Calendar.getInstance().get(Calendar.MONTH)){
            case 0:
                return "Январь";
            case 1:
                return "Февраль";
            case 2:
                return "Март";
            case 3:
                return "Апрель";
            case 4:
                return "Май";
            case 5:
                return "Июнь";
            case 6:
                return "Июль";
            case 7:
                return "Август";
            case 8:
                return "Сентябрь";
            case 9:
                return "Октябрь";
            case 10:
                return "Ноябрь";
            case 11:
                return "Декабрь";
        }

        return "";
    }


    public static String getCurrentDayName(){
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        return new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());

    }

    public static String getCurrentDay(){

        return new SimpleDateFormat("dd/MM/yyyy", new Locale("ru")).format(Calendar.getInstance().getTime());
    }

    public static String[] getDaysOfCurrentWeek(){
        Calendar now = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM",Locale.ENGLISH);

        String[] days = new String[7];

        if(getCurrentDayName().equals("Sunday")) {
            int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) - 5;
            now.add(Calendar.DAY_OF_MONTH, delta);
            for (int i = 0; i < 7; i++) {
                days[i] = format.format(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }
        }else{
            int delta = -now.get(GregorianCalendar.DAY_OF_WEEK) + 2;
            now.add(Calendar.DAY_OF_MONTH, delta);
            for (int i = 0; i < 7; i++) {
                days[i] = format.format(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }
        }

        return days;
    }
}
