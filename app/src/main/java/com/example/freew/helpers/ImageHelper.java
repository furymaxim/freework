package com.example.freew.helpers;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.util.UUID;

public class ImageHelper {


    public static File createImageFile(Context context, String prefix, String ext) throws Throwable {
        String code = UUID.randomUUID().toString();
        code = code.replace("-", "");
        code = code.replace(" ", "");
        code = code.replace("_", "");

        String imageFileName = prefix + "" + code + "";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (storageDir != null) {
            boolean ignore = storageDir.mkdirs();
            return File.createTempFile(imageFileName, "." + ext, storageDir);
        } else {
            return null;
        }
    }
}
