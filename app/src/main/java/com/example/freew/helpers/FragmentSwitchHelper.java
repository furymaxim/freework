package com.example.freew.helpers;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.freew.R;

public class FragmentSwitchHelper {

    private static FragmentManager fragmentManager;
    private static FragmentTransaction fragmentTransaction;

    public static void switchFragmentWithBackStack(Context context, Fragment fragment){
        if(context instanceof FragmentActivity){
            fragmentManager  =  ((FragmentActivity) context).getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

    public static void switchFragmentWithoutBackStack(Context context, Fragment fragment){
        if(context instanceof FragmentActivity){
            fragmentManager  =  ((FragmentActivity) context).getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }


    public static void switchFragmentMainWithoutBackStack(Context context, Fragment fragment){
        if(context instanceof FragmentActivity){
            fragmentManager  =  ((FragmentActivity) context).getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .replace(R.id.fragment, fragment)
                    .commit();
        }
    }
}

