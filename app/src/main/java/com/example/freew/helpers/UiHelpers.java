package com.example.freew.helpers;

import android.content.Context;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.freew.R;

import ru.tinkoff.decoro.MaskImpl;
import ru.tinkoff.decoro.slots.PredefinedSlots;
import ru.tinkoff.decoro.slots.Slot;
import ru.tinkoff.decoro.watchers.MaskFormatWatcher;

public class UiHelpers {

    public static final Slot[] RUS_PHONE_NUMBER_WITH_OUT_SEVEN = {
            PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.hardcodedSlot('(').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot(')').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.hardcodedSlot(' ').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot('-').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot('-').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
    };

    public static final Slot[] DATE_MASK = {
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot('.').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.hardcodedSlot('.').withTags(Slot.TAG_DECORATION),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
            PredefinedSlots.digit(),
    };

    public static void installDateMaskOn(EditText editText) {
        MaskImpl mask = new MaskImpl(UiHelpers.DATE_MASK, true);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        watcher.installOn(editText);
    }

    public static void installPhoneMaskOn(EditText editText) {
        MaskImpl mask = new MaskImpl(UiHelpers.RUS_PHONE_NUMBER_WITH_OUT_SEVEN, true);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        watcher.installOn(editText);
    }

    public static MaterialDialog createProgressDialog(Context context) {
        return new MaterialDialog.Builder(context)
                .title(R.string.loading)
                .content(R.string.please_wait)
                .cancelable(false)
                .progress(true, 0).build();
    }

}
