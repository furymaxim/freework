package com.example.freew.helpers;

import android.content.Context;
import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;

public class DateHelper {
    private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss Z";
    private static final String BIRTHDAY_FORMAT = "dd.MM.yyyy";

    private static Date parseTimeUtcToDateCustomPattern(String time, String pattern) {
        try {
            SimpleDateFormat input = new SimpleDateFormat(pattern, Locale.US);
            input.setTimeZone(TimeZone.getTimeZone("UTC"));
            return input.parse(time);
        } catch (Throwable ignored) {

        }
        return null;
    }

    private static Date parseTimeUtcToDate(String time) {
        try {
            SimpleDateFormat input = new SimpleDateFormat(DEFAULT_FORMAT, Locale.US);
            input.setTimeZone(TimeZone.getTimeZone("UTC"));
            return input.parse(time);
        } catch (Throwable ignored) {

        }
        return null;
    }

    private static String formatDate(Date input, String pattern) {
        if (input == null) {
            return null;
        }
        try {
            SimpleDateFormat output = new SimpleDateFormat(pattern, Locale.getDefault());
            return output.format(input);
        } catch (Throwable ignored) {

        }
        return null;
    }



    private static Calendar parseDateString(String date) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat parser = new SimpleDateFormat(BIRTHDAY_FORMAT, Locale.US);
        parser.setLenient(false);
        try {
            calendar.setTime(parser.parse(date));
        } catch (ParseException e) {
        }
        return calendar;
    }

    public static boolean isValidBirthday(String birthday) {
        Calendar calendar = parseDateString(birthday);
        int year = calendar.get(Calendar.YEAR);
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        return year >= 1900 && year < thisYear;
    }
}
