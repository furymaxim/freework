package com.example.freew.helpers;


public interface ServiceListener<T>{
	void result(T result);
}
