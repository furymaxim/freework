package com.example.freew.ui.base

interface LoaderShower {

    fun showLoader()

    fun hideLoader()
}