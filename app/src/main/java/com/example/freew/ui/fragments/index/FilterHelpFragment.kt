package com.example.freew.ui.fragments.index


import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.TextView
import androidx.fragment.app.DialogFragment

import com.example.freew.R


class FilterHelpFragment : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {


        val view = inflater.inflate(R.layout.fragment_filter_help, container, false)

        val btnOk = view.findViewById<TextView>(R.id.btnOk)

        btnOk.setOnClickListener { dismiss() }

        return view
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        dialog.window!!.setBackgroundDrawableResource(R.drawable.a_gradient_help)

        return dialog
    }


}
