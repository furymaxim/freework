package com.example.freew.ui.fragments.login_group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.freew.R
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.app_first_fragment_v2.*

import kotlinx.android.synthetic.main.app_first_fragment_v2.view.*

class AppFirstV2Fragment : BaseFragment(){


    override val layoutId: Int
        get() = R.layout.app_first_fragment_v2

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findPeople.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, PhoneInputFragment.newInstance())
        }


    }

    companion object {
        fun newInstance(): AppFirstV2Fragment {
            val args = Bundle()
            val fragment = AppFirstV2Fragment ()
            fragment.arguments = args
            return fragment
        }
    }



}
