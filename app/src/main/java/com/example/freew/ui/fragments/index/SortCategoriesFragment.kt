package com.example.freew.ui.fragments.index


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.freew.R
import com.example.freew.adapters.SortListAdapter
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.misc.Constants
import com.example.freew.models.SortOption
import kotlinx.android.synthetic.main.fragment_sort_companies.*
import kotlinx.android.synthetic.main.header_common.*
import kotlin.apply


class SortCategoriesFragment : Fragment() {

    private var itemsSortBy: ArrayList<SortOption>? = null
    var adapter: SortListAdapter? = null
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_sort_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = activity!!.getSharedPreferences(Constants.preference, Context.MODE_PRIVATE)

        setupList()

        checkboxAll.isChecked = true

        apply.setOnClickListener {
            showResult()
            val selectedCount = adapter!!.selectedItems.size
            sharedPreferences!!.edit().putInt("sortByCategories",selectedCount).apply()
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, FilterFragment.newInstance())

        }

        listView.setOnItemClickListener { _, _, position, _ ->
            val item: SortOption = itemsSortBy!![position]

            if(item.isSelected){
                item.isSelected = false
                if(adapter!!.selectedItems.size == 0){
                    checkboxAll.isChecked = true
                }
            }else{
                checkboxAll.isChecked = false
                item.isSelected = true
            }

            itemsSortBy!!.set(position,item)

            adapter!!.updateSortOptions(itemsSortBy)

        }

        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, FilterFragment.newInstance())
        }


        reset.setOnClickListener {
            adapter!!.unselectedItems()
            checkboxAll.isChecked = true
        }


/*
        checkBoxAll.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                for (i in 0 until itemsSortBy!!.size step 1) {
                    val sortOption = itemsSortBy!!.get(i)
                    sortOption.isSelected = false

                    itemsSortBy!!.set(i,sortOption)

                }
            }

            adapter.updateSortOptions(itemsSortBy)
        }
*/

/*

        listView.setOnItemClickListener { parent, view, position, id ->
   */
/*         if(checkBoxAll.isChecked){
                checkBoxAll.isChecked = false
            }
            *//*

        */
/*    val list: ArrayList<SortOption>  = adapter.selectedItems
            if(list.isEmpty()){
                checkBoxAll.isChecked = true
            }else{
                checkBoxAll.isChecked = false
            }*//*



            Toast.makeText(context, position.toString() + " fragment", Toast.LENGTH_SHORT).show()

        }

*/
    }

    private fun setupList(){
        itemsSortBy = generateItemsList()
        adapter = SortListAdapter(context, itemsSortBy)
        listView.adapter = adapter
    }

    private fun generateItemsList():ArrayList<SortOption>{
        val items:ArrayList<SortOption> = ArrayList()

        items.add(SortOption("Custom ListView Item 1",false))
        items.add(SortOption("Категория 2",false))
        items.add(SortOption("Категория 3",false))
        items.add(SortOption("Категория 4",false))
        items.add(SortOption("Категория 5",false))
        items.add(SortOption("Категория 6",false))
        items.add(SortOption("Custom ListView Item 7",false))

        return items

    }

    private fun showResult() {
        var result = "Выбраны категории:"
        for (p in adapter!!.selectedItems) {
            if (p.isSelected)
                result += "\n" + p.title
        }
        Toast.makeText(context, result, Toast.LENGTH_LONG).show()
    }


}



