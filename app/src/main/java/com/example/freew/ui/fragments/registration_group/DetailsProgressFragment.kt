package com.example.freew.ui.fragments.registration_group


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log

import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder

import com.example.freew.misc.Constants.myPreference
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_details_progress.*
import kotlinx.android.synthetic.main.header_common.*


class DetailsProgressFragment : BaseFragment(), View.OnClickListener{


    private var bikCorrect = false
    private var persCorrect = false
    private var settleCorrect = false
    private var settleCardCorrect = false

    override val layoutId: Int
        get() = R.layout.fragment_details_progress

    private var sharedPreferences: SharedPreferences? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = activity!!.getSharedPreferences(myPreference, Context.MODE_PRIVATE)


        pers.setOnClickListener(null)
        settl.setOnClickListener(this)

        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
        }


        detailsBik.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteBik.visibility = View.VISIBLE
                }else{
                    deleteBik.visibility = View.GONE
                }

                if(s.length>8) bikCorrect = true else bikCorrect = false


                isBikAndPersCorrect()

                isBikAndSettleAndSettleCardCorrect()

            }
        })



        detailsPers.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deletePers.visibility = View.VISIBLE
                }else{
                    deletePers.visibility = View.GONE
                }

                if (s.length > 19 && s.indexOf("40817") == 0) persCorrect = true else persCorrect = false



                isBikAndPersCorrect()
            }
        })


        detailsSettl.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteSettl.visibility = View.VISIBLE
                }else{
                    deleteSettl.visibility = View.GONE
                }

                if(s.length > 19 && s.indexOf("3") == 0) settleCorrect = true else settleCorrect = false
                isBikAndSettleAndSettleCardCorrect()

            }
        })


        detailsCardNo.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteCardNo.visibility = View.VISIBLE
                }else{
                    deleteCardNo.visibility = View.GONE
                }

                if(s.length > 12) settleCardCorrect = true else settleCardCorrect = false

                isBikAndSettleAndSettleCardCorrect()

            }
        })

        deleteBik.setOnClickListener {
            detailsBik.setText("")
            bikCorrect = false
            deleteBik.visibility = View.GONE
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
            saveDetails.setOnClickListener(null)
        }

        deletePers.setOnClickListener {
            detailsPers.setText("")
            persCorrect = false
            deletePers.visibility = View.GONE
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
            saveDetails.setOnClickListener(null)
        }

        deleteSettl.setOnClickListener {
            detailsSettl.setText("")
            settleCorrect = false
            deleteSettl.visibility = View.GONE
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
            saveDetails.setOnClickListener(null)
        }

        deleteCardNo.setOnClickListener {
            detailsCardNo.setText("")
            settleCorrect = false
            deleteCardNo.visibility = View.GONE
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
            saveDetails.setOnClickListener(null)
        }

     }

    override fun onClick(v: View?) {
        when(v){
            settl->{
                persCorrect = false
                detailsPers.setText("")
                llPersAcc.visibility = View.GONE
                divPers.visibility = View.GONE
                persHint.visibility = View.GONE
                settl!!.background = context!!.getDrawable(R.drawable.a_button_corners_blue)
                pers!!.background = context!!.getDrawable(R.drawable.a_button_corners_grey)
                saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
                saveDetails.setOnClickListener(null)

                llSettleAcc!!.visibility = View.VISIBLE
                llCardNo!!.visibility = View.VISIBLE
                divSettl!!.visibility = View.VISIBLE
                divSettleNo!!.visibility = View.VISIBLE
                tvSettleHint!!.visibility = View.VISIBLE
                pers.setOnClickListener(this)
                settl.setOnClickListener(null)
            }
            pers->{
                settleCorrect = false
                settleCardCorrect = false
                detailsSettl.setText("")
                detailsCardNo.setText("")
                llSettleAcc!!.visibility = View.GONE
                llCardNo!!.visibility = View.GONE
                divSettl!!.visibility = View.GONE
                divSettleNo!!.visibility = View.GONE
                tvSettleHint!!.visibility = View.GONE

                pers!!.background = context!!.getDrawable(R.drawable.a_button_corners_blue)
                settl!!.background = context!!.getDrawable(R.drawable.a_button_corners_grey)
                saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
                saveDetails.setOnClickListener(null)

                llPersAcc.visibility = View.VISIBLE
                divPers.visibility = View.VISIBLE
                persHint.visibility = View.VISIBLE
                pers.setOnClickListener(null)
                settl.setOnClickListener(this)


            }
        }
    }

    private fun isBikAndPersCorrect(){
        if(bikCorrect && persCorrect){
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_900))
            saveDetails.setOnClickListener{
                sharedPreferences!!.edit().putBoolean("detailsCheckSuccess",true).apply()
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
            }
        }else{
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
            saveDetails.setOnClickListener(null)
        }
    }

    private fun isBikAndSettleAndSettleCardCorrect(){
        if(bikCorrect && settleCorrect && settleCardCorrect){
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_900))
            saveDetails.setOnClickListener{
                sharedPreferences!!.edit().putBoolean("detailsCheckSuccess",true).apply()
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
            }
        }else{
            saveDetails.setTextColor(resources.getColor(R.color.colorBlue_500))
            saveDetails.setOnClickListener(null)
        }
    }

}



