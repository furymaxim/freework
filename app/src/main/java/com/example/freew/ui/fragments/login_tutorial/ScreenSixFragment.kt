package com.example.freew.ui.fragments.login_tutorial


import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_screen_one.*


class ScreenSixFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.fragment_screen_six


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goNext.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, PrepareForRegistrationFragment.newInstance())
        }
    }

    companion object {
        fun newInstance(): ScreenSixFragment {
            val args = Bundle()
            val fragment = ScreenSixFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
