package com.example.freew.ui.fragments.login_tutorial


import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.ui.fragments.registration_group.RegistrationStepsFragment
import com.example.freew.helpers.FragmentSwitchHelper

import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_prepare_for_registration.*
import kotlinx.android.synthetic.main.fragment_screen_one.*


class PrepareForRegistrationFragment : BaseFragment(), View.OnClickListener{

    override val layoutId: Int
        get() = R.layout.fragment_prepare_for_registration


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        startReg.setOnClickListener(this)
        startWithoutReg.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v){
            startReg, startWithoutReg -> FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
        }
    }

    companion object {
        fun newInstance(): PrepareForRegistrationFragment {
            val args = Bundle()
            val fragment = PrepareForRegistrationFragment()
            fragment.arguments = args
            return fragment
        }


    }

}
