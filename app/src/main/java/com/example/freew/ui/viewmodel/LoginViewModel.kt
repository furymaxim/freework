package com.example.freew.ui.viewmodel

import com.example.freew.api.PhoneResponse
import com.example.freew.api.SessionResponse
import com.example.freew.api.WebApi
import com.example.freew.helpers.DateHelper
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class LoginViewModel : BaseViewModel() {
    val phone: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isPhoneValid: Observable<Boolean> = phone.map { it.isNotBlank() && it.length == 15 }
    var phoneRequest = WebApi.getInstance().service.sendPhone()

    val birthday: BehaviorRelay<String> = BehaviorRelay.createDefault("")

    val isBirthValid: Observable<Boolean> = birthday.map {
        it.isNotEmpty() && it.length == 10 && DateHelper.isValidBirthday(it)
    }

    val code: BehaviorRelay<String> = BehaviorRelay.createDefault("")


    val isCodeValid: Observable<Boolean> = code.map { it.isNotEmpty() }

    val loginRequest: Observable<SessionResponse> = standardRequest(Observable.just(SessionResponse()).delay(2, TimeUnit.SECONDS))

    val smsCodeRequest: Observable<PhoneResponse> = standardRequest(Observable.just(PhoneResponse()).delay(2, TimeUnit.SECONDS))
}