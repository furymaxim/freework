package com.example.freew.ui.fragments.index


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CheckBox

import com.example.freew.R
import com.example.freew.adapters.SortListAdapter
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.misc.Constants
import com.example.freew.models.SortOption
import kotlinx.android.synthetic.main.fragment_sort_by.*
import kotlinx.android.synthetic.main.header_common.*


class SortByFragment : Fragment() {

    private var sharedPreferences: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_sort_by, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = activity!!.getSharedPreferences(Constants.preference, Context.MODE_PRIVATE)

        if(!sharedPreferences!!.contains("finalSortBy")){
            checkboxOne.isChecked = true
        }else{
            if(sharedPreferences!!.getInt("finalSortBy",0) == 1){
                checkboxOne.isChecked = true
            }

            if(sharedPreferences!!.getInt("finalSortBy",0) == 2){
                checkboxTwo.isChecked = true
            }

            if(sharedPreferences!!.getInt("finalSortBy",0) == 3){
                checkboxThree.isChecked = true
            }

            if(sharedPreferences!!.getInt("finalSortBy",0) == 4){
                checkboxFour.isChecked = true
            }
        }


        checkboxOne.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                checkBoxAction(checkboxOne, isChecked)
            }
        }


        checkboxTwo.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                checkBoxAction(checkboxTwo, isChecked)
            }
        }

        checkboxThree.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                checkBoxAction(checkboxThree, isChecked)
            }
        }

        checkboxFour.setOnCheckedChangeListener { _, isChecked ->
            if(isChecked) {
                checkBoxAction(checkboxFour, isChecked)
            }
        }

        apply.setOnClickListener {
            if(checkboxOne.isChecked){
                putInShared(1)
            }

            if(checkboxTwo.isChecked){
                putInShared(2)
            }

            if(checkboxThree.isChecked){
                putInShared(3)
            }

            if(checkboxFour.isChecked){
                putInShared(4)
            }

            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, FilterFragment.newInstance())
        }




        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, FilterFragment.newInstance())
        }

    }


    private fun checkBoxAction(cb: CheckBox, isChecked: Boolean){
        deselectAll()
        cb.isChecked = isChecked
        enableAll()
        cb.isEnabled = false
    }

    private fun putInShared(int: Int){
        sharedPreferences!!.edit().putInt("sortBy",int).apply()
    }

    private fun enableAll(){
        checkboxOne.isEnabled = true
        checkboxTwo.isEnabled = true
        checkboxThree.isEnabled = true
        checkboxFour.isEnabled = true
    }

    private fun deselectAll(){
        checkboxOne.isChecked = false
        checkboxTwo.isChecked = false
        checkboxThree.isChecked = false
        checkboxFour.isChecked = false
    }
}
