package com.example.freew.ui.fragments.login_group

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.freew.R
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.ui.activities.AppActivity
import com.example.freew.ui.base.BaseFragment

import kotlinx.android.synthetic.main.app_first_fragment.*
import kotlinx.android.synthetic.main.app_first_fragment.view.*

class AppFirstFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.app_first_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startToEarn.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, PhoneInputFragment.newInstance())
        }

        loginTitle.setOnClickListener {
            val intent = Intent(context, AppActivity::class.java)
            startActivity(intent)
        }
    }




    companion object {
        fun newInstance(): AppFirstFragment {
            val args = Bundle()
            val fragment = AppFirstFragment ()
            fragment.arguments = args
            return fragment
        }
    }


}
