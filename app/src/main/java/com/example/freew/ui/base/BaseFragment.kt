package com.example.freew.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import com.example.freew.R
import com.example.freew.extensions.showErrorDialog

import com.trello.rxlifecycle2.android.FragmentEvent
import com.trello.rxlifecycle2.components.support.RxFragment
import com.trello.rxlifecycle2.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

abstract class BaseFragment : RxFragment(), ErrorShower {
    @get:LayoutRes
    protected abstract val layoutId: Int

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(layoutId, container, false)

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        applyInitialModelValues()
        bindToViewModel()
    }

    protected open fun applyInitialModelValues() = Unit

    open fun bindToViewModel() {}


    fun <T> bindRequest(action: Observable<T>): Observable<T> {
        return action.observeOn(AndroidSchedulers.mainThread()).doOnError(::showError)
            .bindUntilEvent(this, FragmentEvent.DESTROY_VIEW)
    }

    fun <T> bindRequest(action: Observable<T>, errorShower: ErrorShower): Observable<T> {
        return action.observeOn(AndroidSchedulers.mainThread()).doOnError { errorShower.showError(it) }
            .bindUntilEvent(this, FragmentEvent.DESTROY_VIEW)
    }

    override fun showError(error: Throwable) {
        context?.showErrorDialog(error)
    }

    protected fun navigateNext(fragment: BaseFragment) {
        fragmentManager!!.beginTransaction()
            .replace(R.id.fragment_container, fragment)
            .setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
            .addToBackStack(null)
            .commitAllowingStateLoss()
    }


    private fun getBaseActivity(): BaseActivity? {
        val act = activity
        return if (act != null) {
            activity as BaseActivity
        } else {
            null
        }
    }

}
