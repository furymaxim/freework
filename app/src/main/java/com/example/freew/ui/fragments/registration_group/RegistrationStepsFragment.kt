package com.example.freew.ui.fragments.registration_group


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle

import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.example.freew.R
import com.example.freew.R2

import com.example.freew.ui.fragments.login_tutorial.PrepareForRegistrationFragment
import com.example.freew.helpers.FragmentSwitchHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.misc.Constants.myPreference
import com.example.freew.ui.activities.AppActivity
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.bottom_sheet.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.fragment_prepare_for_registration.*


class RegistrationStepsFragment : BaseFragment(), View.OnClickListener{


    override val layoutId: Int
        get() = R.layout.fragment_registration_steps

    private var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    private var sharedPreferences: SharedPreferences? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
        sharedPreferences = activity!!.getSharedPreferences(myPreference, Context.MODE_PRIVATE)


        if(isAllSuccess()){
            val intent = Intent(context, AppActivity::class.java)
            startActivity(intent)
        }



        checkDidUserAcceptAgreement()

        doesPassportCompleted()
        doesInnCompleted()
        doesContractCompleted()
        doesDetailsCompleted()



        goPrev.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, PrepareForRegistrationFragment.newInstance())
        }



        cbAccept.setOnCheckedChangeListener { _ , isChecked ->

            if (!isChecked) {
                llPas.setOnClickListener(null)
                llInn.setOnClickListener(null)
                llContract.setOnClickListener(null)
                llDetails.setOnClickListener(null)
            }else{
                sharedPreferences!!.edit().putBoolean("userAccept", true).apply()
                bottomSheetBehavior!!.isHideable = true
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN
                llPas.setOnClickListener(this)
                llInn.setOnClickListener(this)
                llContract.setOnClickListener(this)
                llDetails.setOnClickListener(this)
            }
        }
    }

    override fun onClick(v: View?) {
        when(v){
            shortDiv->{
                bottomSheetBehavior!!.isHideable = true
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
            }
            llPas -> {
                if (sharedPreferences!!.contains("passportCheckSuccess")) {
                    sharedPreferences!!.edit().putBoolean("passportCheckSuccess", false).apply()
                    //passport!!.background = activity!!.getDrawable(R.drawable.a_button_corners_grey)
                    //bOkPas!!.visibility = View.INVISIBLE
                }
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, PassportProgressFragment())
            }
            llInn -> {
                if (sharedPreferences!!.contains("innCheckSuccess")) {
                    sharedPreferences!!.edit().putBoolean("innCheckSuccess", false).apply()
                    //passport!!.background = activity!!.getDrawable(R.drawable.a_button_corners_grey)
                    //bOkPas!!.visibility = View.INVISIBLE
                }
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, InnProgressFragment())
            }
            llContract -> {
                if (sharedPreferences!!.contains("contractCheckSuccess")) {
                    sharedPreferences!!.edit().putBoolean("contractCheckSuccess", false).apply()
                    //passport!!.background = activity!!.getDrawable(R.drawable.a_button_corners_grey)
                    //bOkPas!!.visibility = View.INVISIBLE
                }
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, ContractProgressFragment())
            }
            llDetails -> {
                if (sharedPreferences!!.contains("detailsCheckSuccess")) {
                    sharedPreferences!!.edit().putBoolean("detailsCheckSuccess", false).apply()
                    //passport!!.background = activity!!.getDrawable(R.drawable.a_button_corners_grey)
                    //bOkPas!!.visibility = View.INVISIBLE
                }
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, DetailsProgressFragment())
            }

        }

    }


    private fun doesPassportCompleted(){
        if (sharedPreferences!!.contains("passportCheckSuccess") && sharedPreferences!!.getBoolean("passportCheckSuccess", false)) {
            llPas!!.background = activity!!.getDrawable(R.drawable.a_button_corners)
            okPas!!.visibility = View.VISIBLE
        }
    }

    private fun doesInnCompleted(){
        if (sharedPreferences!!.contains("innCheckSuccess") && sharedPreferences!!.getBoolean("innCheckSuccess", false)) {
            llInn!!.background = activity!!.getDrawable(R.drawable.a_button_corners)
            okInn!!.visibility = View.VISIBLE
        }
    }

    private fun doesContractCompleted(){
        if (sharedPreferences!!.contains("contractCheckSuccess") && sharedPreferences!!.getBoolean("contractCheckSuccess", false)) {
            llContract!!.background = activity!!.getDrawable(R.drawable.a_button_corners)
            okContract!!.visibility = View.VISIBLE
        }
    }


    private fun doesDetailsCompleted(){
        if (sharedPreferences!!.contains("detailsCheckSuccess") && sharedPreferences!!.getBoolean("detailsCheckSuccess", false)) {
            llDetails!!.background = activity!!.getDrawable(R.drawable.a_button_corners)
            okDetails!!.visibility = View.VISIBLE

        }
    }


    private fun checkDidUserAcceptAgreement(){
        if (sharedPreferences!!.contains("userAccept")) {
            bottomSheetBehavior!!.isHideable = true
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN
            llPas.setOnClickListener(this)
            llInn.setOnClickListener(this)
            llContract.setOnClickListener(this)
            llDetails.setOnClickListener(this)
        }else{
            shortDiv.setOnClickListener(this)
        }
    }

    private fun isAllSuccess(): Boolean{
        return (sharedPreferences!!.getBoolean("detailsCheckSuccess", false) &&  sharedPreferences!!.getBoolean("innCheckSuccess", false) && sharedPreferences!!.getBoolean("passportCheckSuccess", false)  && sharedPreferences!!.getBoolean("contractCheckSuccess", false) )
    }

    companion object {
        fun newInstance(): RegistrationStepsFragment {
            val args = Bundle()
            val fragment =  RegistrationStepsFragment()
            fragment.arguments = args
            return fragment
        }





    }
}
