package com.example.freew.ui.fragments.login_group


import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.example.freew.R
import com.example.freew.R2

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.misc.Parameters
import com.example.freew.ui.base.TitleFragment
import kotlinx.android.synthetic.main.fragment_login_fail.*
import kotlinx.android.synthetic.main.fragment_phone_confirmation.*
import java.text.DecimalFormat


class LoginFailFragment : TitleFragment(){

    override val layoutId = R.layout.fragment_login_fail


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        val unformattedPhone = arguments?.getString(Parameters.PHONE)
        val formattedPhone = unformattedPhone?.substring(0,2) + " " + unformattedPhone?.substring(3,5) + " " + unformattedPhone?.substring(6,7)+ " " +unformattedPhone?.substring(8,unformattedPhone?.length)


        containerText.text = "Пользователь с номером телефона\n+7 $formattedPhone при регистрации\nуказал другую дату рождения."


        closeFail.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, AppFirstFragment.newInstance())
        }


    }


    companion object {
        fun newInstance(phone: String): LoginFailFragment {
            val args = Bundle()
            args.putString(Parameters.PHONE, phone)
            val fragment = LoginFailFragment()
            fragment.arguments = args
            return fragment
        }
    }


}
