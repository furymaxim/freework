package com.example.freew.ui.fragments.index


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.freew.R
import com.example.freew.misc.Constants.preference
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_home.*

import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.freew.adapters.TaskAdapter
import com.example.freew.helpers.*
import com.example.freew.models.ServerFake
import com.example.freew.models.Task
import com.example.freew.ui.activities.AppActivity
import com.google.android.material.snackbar.Snackbar
import java.util.ArrayList
import androidx.core.view.ViewCompat.animate
import android.R.attr.translationY
import android.opengl.ETC1.getHeight
import android.widget.FrameLayout

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.trello.rxlifecycle2.RxLifecycle.bindUntilEvent





class HomeFragment : BaseFragment(), TaskAdapter.OnTaskListener, RecyclerItemTouchHelperListener{

    var isMondayAvailable:Boolean = true
    var isTuesdayAvailable:Boolean = true
    var isWednesdayAvailable:Boolean = true
    var isThursdayAvailable:Boolean = true
    var isFridayAvailable:Boolean = true
    var isSaturdayAvailable:Boolean = true

    private var taskList = ArrayList<Task>()
    var taskAdapter = TaskAdapter(this)

    var height = 0


    private var sharedPreferences: SharedPreferences? = null



    override val layoutId: Int
        get() = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sharedPreferences = activity!!.getSharedPreferences(preference, Context.MODE_PRIVATE)

        height = appBar.layoutParams.height

        setCurrentMonth()
        setActiveDay()
        setWeekDays()
        setInactiveDays(CalendarRowHelper.getCurrentDayName())

        val activity = getActivity() as AppActivity

        activity.showBottomNavigationView()

        if(!isTasksAvailableToday()){
            recyclerView.visibility = View.GONE
            noTasks.visibility = View.VISIBLE
        }


        filter.setOnClickListener {
            if (!sharedPreferences!!.contains("filterClicked")) {
                val newFragment: DialogFragment? = FilterHelpFragment()

                newFragment?.show(fragmentManager!!, "dialog")

                sharedPreferences!!.edit().putBoolean("filterClicked", true).apply()
            }else{
                sharedPreferences!!.edit().putBoolean("fromMain", true).apply()
                FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, FilterFragment.newInstance())
            }
        }

        initRecyclerView()

        if(!sharedPreferences!!.contains("filterTask")) {
            taskList = ServerFake.getTasks()
        }else{
            taskList = ServerFake.getTasks()
        }

        taskAdapter.setItems(taskList)

    }

    private fun initRecyclerView(){
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.itemAnimator = DefaultItemAnimator()
        //recyclerView.setNestedScrollingEnabled(false);
        //recyclerView.addItemDecoration(LayoutMarginDecoration(1, 2))


/*        if(recyclerView.canScrollVertically(-1)){
            appBar.visibility = View.GONE
        } else {
            appBar.visibility = View.VISIBLE
        }*/


        val height = appBar.layoutParams.height
        val params = appBar.layoutParams




        recyclerView.addOnScrollListener(object:RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if(recyclerView.canScrollVertically(-1)){
                    hideViews()
                    params.height = 0
                    appBar.layoutParams = params

                } else {
                    showViews()
                    params.height = height
                    appBar.layoutParams = params
                }
            }
        })



        recyclerView.adapter = taskAdapter



        val itemTouchHelperCallback: ItemTouchHelper.SimpleCallback =
                RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT, this)

        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView)


    }

    private fun hideViews() {
        appBar.animate().translationY(appBar.getHeight() + 70f).setInterpolator(AccelerateInterpolator(2f)).start()


    }

    private fun showViews() {
        appBar.animate().translationY(0f).setInterpolator(DecelerateInterpolator(2f)).start()

    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int){
        val name: String = taskList.get(viewHolder.adapterPosition).title
        val deletedItem = taskList.get(viewHolder.adapterPosition)

        val deleteIndex  = viewHolder.adapterPosition

        taskAdapter.removeItem(deleteIndex)


        val snackBar: Snackbar = Snackbar.make(rootLayout,"Задача «$name» удалена",Snackbar.LENGTH_LONG)

        snackBar.setTextColor(resources.getColor(R.color.colorBlue_900))
        snackBar.setAction("Вернуть", View.OnClickListener {
            taskAdapter.restoreItem(deletedItem, deleteIndex)
        })
        snackBar.setBackgroundTint(resources.getColor(R.color.grey_bottom))
        snackBar.setActionTextColor(resources.getColor(R.color.colorBlue_900))
        snackBar.show()

    }



    private fun isTasksAvailableToday():Boolean{
        val isAvailable = null

        return true
    }


    private fun setWeekDays(){
        val days = CalendarRowHelper.getDaysOfCurrentWeek()

        numberMon.text = (days[0].substring(0,2))
        numberTue.text = (days[1].substring(0,2))
        numberWed.text = (days[2].substring(0,2))
        numberThu.text = (days[3].substring(0,2))
        numberFri.text = (days[4].substring(0,2))
        numberSat.text = (days[5].substring(0,2))
        numberSun.text = (days[6].substring(0,2))
    }


    private fun setActiveDay(){

        when(CalendarRowHelper.getCurrentDayName()){
            "Monday" ->{
                dayMon.setBackgroundResource(R.drawable.selected_day)
                nameMon.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberMon.setTextColor(resources.getColor(R.color.colorWhite))
            }
            "Tuesday" ->{
                dayTue.setBackgroundResource(R.drawable.selected_day)
                nameTue.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberTue.setTextColor(resources.getColor(R.color.colorWhite))
            }
            "Wednesday" ->{
                dayWed.setBackgroundResource(R.drawable.selected_day)
                nameWed.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberWed.setTextColor(resources.getColor(R.color.colorWhite))
            }
            "Thursday" ->{
                dayThu.setBackgroundResource(R.drawable.selected_day)
                nameThu.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberThu.setTextColor(resources.getColor(R.color.colorWhite))
            }
            "Friday" ->{
                dayFri.setBackgroundResource(R.drawable.selected_day)
                nameFri.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberFri.setTextColor(resources.getColor(R.color.colorWhite))
            }
            "Saturday" ->{
                daySat.setBackgroundResource(R.drawable.selected_day)
                nameSat.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberSat.setTextColor(resources.getColor(R.color.colorWhite))
            }
            "Sunday" ->{
                daySun.setBackgroundResource(R.drawable.selected_day)
                nameSun.setTextColor(resources.getColor(R.color.colorBlue_500))
                numberSun.setTextColor(resources.getColor(R.color.colorWhite))
            }
        }
    }


    private fun setInactiveDays(currentDayName:String) {
        when (currentDayName) {
            "Monday" -> {

            }
            "Tuesday" -> {
                isMondayAvailable=false
                numberMon.setTextColor(resources.getColor(R.color.colorBlack_600))
            }
            "Wednesday" -> {
                isMondayAvailable=false
                numberMon.setTextColor(resources.getColor(R.color.colorBlack_600))
                isTuesdayAvailable=false
                numberTue.setTextColor(resources.getColor(R.color.colorBlack_600))
            }
            "Thursday" -> {
                isMondayAvailable=false
                numberMon.setTextColor(resources.getColor(R.color.colorBlack_600))
                isTuesdayAvailable=false
                numberTue.setTextColor(resources.getColor(R.color.colorBlack_600))
                isWednesdayAvailable=false
                numberWed.setTextColor(resources.getColor(R.color.colorBlack_600))
            }
            "Friday" -> {
                isMondayAvailable=false
                numberMon.setTextColor(resources.getColor(R.color.colorBlack_600))
                isTuesdayAvailable=false
                numberTue.setTextColor(resources.getColor(R.color.colorBlack_600))
                isWednesdayAvailable=false
                numberWed.setTextColor(resources.getColor(R.color.colorBlack_600))
                isThursdayAvailable=false
                numberThu.setTextColor(resources.getColor(R.color.colorBlack_600))

            }
            "Saturday" -> {
                isMondayAvailable=false
                numberMon.setTextColor(resources.getColor(R.color.colorBlack_600))
                isTuesdayAvailable=false
                numberTue.setTextColor(resources.getColor(R.color.colorBlack_600))
                isWednesdayAvailable=false
                numberWed.setTextColor(resources.getColor(R.color.colorBlack_600))
                isThursdayAvailable=false
                numberThu.setTextColor(resources.getColor(R.color.colorBlack_600))
                isFridayAvailable=false
                numberFri.setTextColor(resources.getColor(R.color.colorBlack_600))
            }
            "Sunday" -> {
                isMondayAvailable=false
                numberMon.setTextColor(resources.getColor(R.color.colorBlack_600))
                isTuesdayAvailable=false
                numberTue.setTextColor(resources.getColor(R.color.colorBlack_600))
                isWednesdayAvailable=false
                numberWed.setTextColor(resources.getColor(R.color.colorBlack_600))
                isThursdayAvailable=false
                numberThu.setTextColor(resources.getColor(R.color.colorBlack_600))
                isFridayAvailable=false
                numberFri.setTextColor(resources.getColor(R.color.colorBlack_600))
                isSaturdayAvailable=false
                numberSat.setTextColor(resources.getColor(R.color.colorBlack_600))
            }
        }
    }

    private fun setCurrentMonth(){
        month.setText(CalendarRowHelper.getCurrentMonth())
    }


    override fun onTaskClick(position: Int) {
            val currentPos = taskAdapter.taskList.get(position).title
            Toast.makeText(context,currentPos,Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()
            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }





    }
}
