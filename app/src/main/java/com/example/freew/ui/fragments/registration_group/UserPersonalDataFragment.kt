package com.example.freew.ui.fragments.registration_group


import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ScrollView
import android.widget.TextView

import com.example.freew.R
import com.example.freew.misc.Constants

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.example.freew.helpers.DateHelper
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.helpers.UiHelpers
import com.example.freew.ui.base.BaseFragment
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_user_personal_data.*
import kotlinx.android.synthetic.main.header_common.*


class UserPersonalDataFragment : BaseFragment() {


    private var lastNameCorrect = false
    private var firstNameCorrect = false
    private var patronymicCorrect = false
    private var birthDateCorrect = false
    private var sexCorrect = false
    private var addressRawOneCorrect = false
    private var seriesCorrect = false
    private var pasOrgCorrect = false
    private var codeOrgCorrect = false
    private var pasDateCorrect = false

    override val layoutId: Int
        get() = R.layout.fragment_user_personal_data
    private var sharedPreferences: SharedPreferences? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupMaskPasSeries()
        setupMaskCodeOrg()

        UiHelpers.installDateMaskOn(birthDate)
        UiHelpers.installDateMaskOn(pasDate)

        sharedPreferences = activity!!.getSharedPreferences(Constants.myPreference, Context.MODE_PRIVATE)

        lastName.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank())
                    deleteLastName.visibility = View.VISIBLE
                else
                    deleteLastName.visibility = View.GONE

                if(s.length >3) lastNameCorrect = true else lastNameCorrect = false

                isAllCorrect()
            }
        })


        firstName.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteFirstName.visibility = View.VISIBLE

                }else{
                    deleteFirstName.visibility = View.GONE
                }

                if(s.length > 2) firstNameCorrect = true else firstNameCorrect = false

                isAllCorrect()
            }
        })


        patronymic.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deletePatronymic.visibility = View.VISIBLE

                }else{
                    deletePatronymic.visibility = View.GONE
                }

                if(s.length > 5) patronymicCorrect = true else patronymicCorrect = false

                isAllCorrect()

            }
        })



        birthDate.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {


                if(s!!.isNotEmpty() && s.length == 10 && DateHelper.isValidBirthday(birthDate.text.toString())) birthDateCorrect = true else birthDateCorrect = false


                isAllCorrect()

            }
        })



        sex.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank())
                    deleteSex.visibility = View.VISIBLE
                else
                    deleteSex.visibility = View.GONE


                if(s.isNotEmpty() && (s.indexOf("м") > -1  || s.indexOf("ж") > -1)) sexCorrect = true else sexCorrect = false

                isAllCorrect()
            }
        })


        addressRowOne.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteAddressRowOne.visibility = View.VISIBLE

                }else{
                    deleteAddressRowOne.visibility = View.GONE
                }

                if(s.length > 15) addressRawOneCorrect = true else addressRawOneCorrect = false


                if(s.length > 20){
                    addressRowTwo.isEnabled = true
                } else {
                    addressRowTwo.setText("")
                    addressRowThree.setText("")
                    addressRowTwo.isEnabled = false
                }

                isAllCorrect()
            }
        })


        addressRowTwo.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(s!!.isNotBlank()){
                    deleteAddressRowTwo.visibility = View.VISIBLE

                }else{
                    deleteAddressRowTwo.visibility = View.GONE
                }

                if(s.length > 20){
                    addressRowThree.isEnabled = true
                } else {
                    addressRowThree.setText("")
                    addressRowThree.isEnabled = false
                }

                isAllCorrect()
            }
        })



        addressRowThree.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank())
                    deleteAddressRowThree.visibility = View.VISIBLE
                else
                    deleteAddressRowThree.visibility = View.GONE



                isAllCorrect()
            }
        })

        pasOrgRowOne.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank())
                    deletePasOrgRowOne.visibility = View.VISIBLE
                else
                    deletePasOrgRowOne.visibility = View.GONE


                if(s.length > 10){
                    pasOrgCorrect = true
                }else{
                    pasOrgCorrect = false
                }

                if(s.length > 20){
                    pasOrgRowTwo.isEnabled = true
                } else {
                    pasOrgRowTwo.setText("")
                    pasOrgRowTwo.isEnabled = false
                }

                isAllCorrect()
            }
        })


        pasOrgRowTwo.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank())
                    deletePasOrgRowTwo.visibility = View.VISIBLE
                else
                    deletePasOrgRowTwo.visibility = View.GONE



                isAllCorrect()
            }
        })




        pasNumber.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(s!!.isNotBlank() && s.length > 11) seriesCorrect = true else seriesCorrect = false

                isAllCorrect()
            }
        })

        pasCode.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(s!!.isNotEmpty() && s.length > 6) codeOrgCorrect = true else codeOrgCorrect = false

                isAllCorrect()
            }
        })


        pasDate.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(s!!.isNotEmpty() && s.length > 9) pasDateCorrect = true else pasDateCorrect = false

                isAllCorrect()
            }
        })


        deleteLastName.setOnClickListener{
            lastName.setText("")
            lastNameCorrect = false
            deleteLastName.visibility = View.GONE
            setFailSavePas()
        }

        deleteFirstName.setOnClickListener{
            firstName.setText("")
            firstNameCorrect = false
            deleteFirstName.visibility = View.GONE
            setFailSavePas()
        }

        deletePatronymic.setOnClickListener{
            patronymic.setText("")
            patronymicCorrect = false
            deletePatronymic.visibility = View.GONE
            setFailSavePas()
        }

        deleteSex.setOnClickListener{
            sex.setText("")
            sexCorrect = false
            deleteSex.visibility = View.GONE
            setFailSavePas()
        }

        deleteAddressRowOne.setOnClickListener{
            addressRowOne.setText("")
            addressRawOneCorrect = false
            deleteAddressRowOne.visibility = View.GONE
            setFailSavePas()
        }

        deleteAddressRowTwo.setOnClickListener{
            addressRowTwo.setText("")
            deleteAddressRowTwo.visibility = View.GONE
        }

        deleteAddressRowThree.setOnClickListener{
            addressRowThree.setText("")
            deleteAddressRowThree.visibility = View.GONE
        }


        deletePasOrgRowOne.setOnClickListener{
            pasOrgRowOne.setText("")
            pasOrgCorrect = false
            deletePasOrgRowOne.visibility = View.GONE
            setFailSavePas()
        }

        deletePasOrgRowTwo.setOnClickListener{
            pasOrgRowTwo.setText("")
            deletePasOrgRowTwo.visibility = View.GONE
        }


        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
        }


    }


    private fun isAllCorrect(){
        if(pasOrgCorrect && lastNameCorrect && firstNameCorrect && patronymicCorrect && birthDateCorrect && sexCorrect && addressRawOneCorrect && seriesCorrect && codeOrgCorrect && pasDateCorrect){
            savePas.setTextColor(resources.getColor(R.color.colorBlue_900))
            savePas.setOnClickListener{
                sharedPreferences!!.edit().putBoolean("passportCheckSuccess",true).apply()
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())

            }
        }else{
            setFailSavePas()
        }
    }

    private fun setFailSavePas(){
        savePas.setTextColor(resources.getColor(R.color.colorBlue_500))
        savePas.setOnClickListener(null)
    }

    private fun setupMaskPasSeries() {
        setupMask(pasNumber, "[00] [00] [000000]")

    }


    private fun setupMaskCodeOrg() {

        setupMask(pasCode, "[000]-[000]")

    }

    private fun setupMask(et: EditText, mask:String){
        val  affineFormats = listOf(mask)


        val listener: MaskedTextChangedListener = MaskedTextChangedListener.Companion.installOn(
                et, mask,
                affineFormats,
                AffinityCalculationStrategy.PREFIX, null)

        et.hint = listener.placeholder()
    }




}
