package com.example.freew.ui.fragments.registration_group

import android.os.Bundle

import androidx.fragment.app.Fragment

import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


import com.example.freew.R
import com.example.freew.helpers.FragmentSwitchHelper

import kotlinx.android.synthetic.main.fragment_passport_check_ongoing.*


class PassportCheckOngoing : Fragment() {


    private var timeLeftInMillis = 10000L

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_passport_check_ongoing, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startTimer()

    }

    private fun startTimer(){ object: CountDownTimer(timeLeftInMillis, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMillis = millisUntilFinished
                updateCountDownText()
            }

            override fun onFinish() {
                checkTimer.text = ""

                if(setFailResult())
                    FragmentSwitchHelper.switchFragmentWithoutBackStack(context,CameraFragment.newInstance())
                else
                    FragmentSwitchHelper.switchFragmentWithoutBackStack(context,UserPersonalDataFragment())

            }
        }.start()
    }

    private fun updateCountDownText() {
        val seconds = (timeLeftInMillis / 1000).toInt()
        val timeLeftFormatted = seconds.toString()
        val text = resources.getString(R.string.time_left_check, timeLeftFormatted)
        checkTimer.text = text
    }

    private fun setFailResult():Boolean = false


    companion object {
        fun newInstance(data: String): PassportCheckOngoing {
            val args = Bundle()
            args.putString("passportData", data)
            val fragment = PassportCheckOngoing()
            fragment.arguments = args
            return fragment
        }

    }
}
