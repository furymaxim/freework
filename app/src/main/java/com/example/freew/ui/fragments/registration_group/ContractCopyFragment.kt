package com.example.freew.ui.fragments.registration_group


import android.os.Bundle

import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper

import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import kotlinx.android.synthetic.main.footer_gradient_white.*


class ContractCopyFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_contract_copy, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        footerOKButton.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment())
        }
    }


}
