package com.example.freew.ui.activities;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.freew.R;
import com.example.freew.ui.fragments.login_group.AppFirstFragment;

public class MainActivity extends FragmentActivity {

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        attachFragment(new AppFirstFragment());
    }


    public void attachFragment(Fragment fragment){
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(R.id.fragment_container, fragment)
                .commitAllowingStateLoss();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
