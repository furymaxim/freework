package com.example.freew.ui.fragments.registration_group;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.freew.R;
import com.example.freew.R2;
import com.example.freew.helpers.ImageHelper;
import com.example.freew.misc.Constants;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.jetbrains.annotations.Nullable;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.FotoapparatBuilder;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.result.BitmapPhoto;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.result.WhenDoneListener;
import io.fotoapparat.selector.FocusModeSelectorsKt;
import io.fotoapparat.selector.SelectorsKt;
import io.fotoapparat.view.CameraView;
import kotlin.Unit;
import static io.fotoapparat.result.transformer.ResolutionTransformersKt.scaled;
import static io.fotoapparat.selector.FocusModeSelectorsKt.autoFocus;
import static io.fotoapparat.selector.FocusModeSelectorsKt.continuousFocusPicture;
import static io.fotoapparat.selector.FocusModeSelectorsKt.fixed;
import static io.fotoapparat.selector.FocusModeSelectorsKt.infinity;

public class CameraFragment extends Fragment {

    @BindView(R.id.camera_view) CameraView cameraView;
    @BindView(R.id.camera_mask)LinearLayout cameraMask;
    @BindView(R.id.root)RelativeLayout relativeLayout;
    private ImageView image;
    private Fotoapparat fotoapparat;
    private Unbinder unbinder;
    private View photoView;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private BottomSheetDialog bottomSheetDialog;
    private View sheetView;
    private final int PERMISSION_REQUEST_CODE = 100;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_camera, container, false);
        unbinder = ButterKnife.bind(this,v);
        photoView = v.findViewById(R.id.photo_btn);


        relativeLayout = v.findViewById(R.id.root);

        if (!checkPermission()){
            requestPermission();
        }



        bottomSheetDialog = new BottomSheetDialog(getContext());
        sheetView = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_camera, null);

        image = sheetView.findViewById(R.id.passport_image);
        bottomSheetDialog.setContentView(sheetView);


        FotoapparatBuilder builder = Fotoapparat.with(getActivity()).into(cameraView).previewScaleType(ScaleType.CenterCrop).focusMode(SelectorsKt.firstAvailable(  // (optional) use the first focus mode which is supported by device
                FocusModeSelectorsKt.continuousFocusPicture(),
                FocusModeSelectorsKt.autoFocus(),
                FocusModeSelectorsKt.fixed()));
        fotoapparat = builder.build();

        photoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTakePicture();
                File f = savePicture();
                askDoYouLikePhoto(f);
            }
        });



        return v;

    }


    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }



    private void requestPermission() {

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,},
                PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext(), "Permission Granted", Toast.LENGTH_SHORT).show();

                    // main logic
                } else {
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @OnClick(R2.id.camera_close)
    void getBack(){
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.fragment_container, new PassportProgressFragment())
                .commit();
    }



    @Override
    public void onStart() {
        super.onStart();
        fotoapparat.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        fotoapparat.stop();
    }





   private void askDoYouLikePhoto(File file){
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();
        cameraMask.setVisibility(View.GONE);

       Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());

       TextView takeAgain;
       TextView goToCheck;
       image.setImageBitmap(myBitmap);



       takeAgain = sheetView.findViewById(R.id.take_photo_again);
       goToCheck = sheetView.findViewById(R.id.go_to_check);

       takeAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoView.setEnabled(true);
                //file.delete();
                cameraMask.setVisibility(View.VISIBLE);
                bottomSheetDialog.dismiss();

            }
        });

        goToCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                // editor.putString("passportPath", file.getAbsolutePath()).apply();

                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .replace(R.id.fragment_container, new PassportCheckOngoing())
                        .commit();

            }
        });

    }


    private File savePicture() {
        PhotoResult photoResult = fotoapparat.takePicture();
        File outputFile = null;
        try {
            outputFile = ImageHelper.createImageFile(getActivity(), "APPARAT", "PNG");
            photoResult.saveToFile(outputFile).whenDone(new WhenDoneListener<Unit>() {
                @Override
                public void whenDone(@Nullable Unit unit) {

                }
            });
            int file_size = Integer.parseInt(String.valueOf(outputFile.length()/1024));
            Toast.makeText(getContext(), String.valueOf(file_size), Toast.LENGTH_SHORT).show();
        } catch (Throwable throwable) {
            Toast.makeText(getContext(), "Произошла ошибка", Toast.LENGTH_SHORT).show();

        }


        return outputFile;
    }




    private void startTakePicture() {
        photoView.setEnabled(false);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public static CameraFragment newInstance() {

        Bundle args = new Bundle();

        CameraFragment fragment = new CameraFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
