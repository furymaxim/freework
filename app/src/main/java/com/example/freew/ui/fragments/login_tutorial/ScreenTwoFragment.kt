package com.example.freew.ui.fragments.login_tutorial


import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_screen_one.*


class ScreenTwoFragment : BaseFragment() {


    override val layoutId: Int
        get() = R.layout.fragment_screen_two


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goNext.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, ScreenThreeFragment.newInstance())
        }
    }

    companion object {
        fun newInstance(): ScreenTwoFragment {
            val args = Bundle()
            val fragment = ScreenTwoFragment()
            fragment.arguments = args
            return fragment
        }

    }
}
