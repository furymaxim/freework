package com.example.freew.ui.fragments.registration_group


import android.Manifest
import android.content.ContentValues.TAG
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log

import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.checkSelfPermission

import com.example.freew.R
import com.example.freew.R2

import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.helpers.FragmentSwitchHelper
import kotlinx.android.synthetic.main.fragment_passport_progress.*
import kotlinx.android.synthetic.main.header_common.*


class PassportProgressFragment : Fragment() {


    private val MY_CAMERA_REQUEST_CODE = 100


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_passport_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val permission = checkSelfPermission(context!!,
                Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }

        buttonAddPhoto.setOnClickListener {

            //if(permission == PackageManager.PERMISSION_GRANTED) {
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, CameraFragment())
            //}else{
            //    makeRequest()
            //}

        }


        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
        }

    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(activity!!,
                arrayOf(Manifest.permission.CAMERA),
                MY_CAMERA_REQUEST_CODE)


    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_CAMERA_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                }
            }
        }
    }




}
