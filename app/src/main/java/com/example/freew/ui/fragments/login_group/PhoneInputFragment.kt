package com.example.freew.ui.fragments.login_group

import android.os.Bundle

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.example.freew.R
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import com.example.freew.extensions.bindClicks
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.ui.base.TitleFragment
import com.example.freew.ui.viewmodel.LoginViewModel
import com.jakewharton.rxbinding2.widget.textChanges
import kotlinx.android.synthetic.main.fragment_phone_input.*
import kotlinx.android.synthetic.main.header_common.*


class PhoneInputFragment : TitleFragment() {
    override val layoutId = R.layout.fragment_phone_input
    private val model = LoginViewModel()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dis1 = phoneView.textChanges().map { text-> text.toString() }.subscribe { model.phone.accept(it) }
        val dis2 = nextView.bindClicks(this, this, model.phone, model.isPhoneValid).subscribe({goNext(it)}, {})

        setupEditText(phoneView)
        setupKeyboardForInput(phoneView)
        setupMask()




        phoneView.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {

                if(s.isNotEmpty() && s.length == 15) {
                    deleteText.visibility = View.VISIBLE
                    nextView.setTextColor(resources.getColor(R.color.colorBlue_900))
                }else if(s.isNotEmpty()){
                    deleteText.visibility = View.VISIBLE
                    nextView.setTextColor(resources.getColor(R.color.colorBlue_500))
                }else{
                    deleteText.visibility = View.INVISIBLE
                    nextView.setTextColor(resources.getColor(R.color.colorBlue_500))
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })



        deleteText.setOnClickListener{
            phoneView?.setText("")
        }


        headerBackButtonView.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, AppFirstV2Fragment.newInstance())
        }
    }

    private fun setupMask() {

        val  affineFormats = listOf("([000]) [000]-[00]-[00]")


        val listener:MaskedTextChangedListener = MaskedTextChangedListener.Companion.installOn(
                phoneView, "([000]) [000]-[00]-[00]",
                affineFormats,
                AffinityCalculationStrategy.PREFIX, null)

         phoneView?.setHint(listener.placeholder())

    }

    private fun goNext(phone: String){
        val fragment = PhoneConfirmationFragment.newInstance(mapPhone(phone))
        navigateNext(fragment)
    }

    private fun mapPhone(phone: String): String {
        var result = phone.replace(" ", "")
        result = result.replace("+", "")
        result = result.replace(")", "")
        result = result.replace("(", "")
        result = result.replace("-", "")
        return "7$result"
    }

    companion object {
        fun newInstance(): PhoneInputFragment {
            val args = Bundle()
            val fragment = PhoneInputFragment ()
            fragment.arguments = args
            return fragment
        }
    }


    private fun setupKeyboardForInput(editText: EditText) {
        val ic = editText.onCreateInputConnection(EditorInfo())
        keyboardInput?.setInputConnection(ic)
    }

    private fun setupEditText(editText: EditText) {
       editText.isFocusable = false
       editText.isClickable = false

    }


}






