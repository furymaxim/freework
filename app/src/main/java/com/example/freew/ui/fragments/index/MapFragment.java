package com.example.freew.ui.fragments.index;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.freew.R;
import com.example.freew.adapters.PlaceAutocompleteAdapter;
import com.example.freew.helpers.BitmapDescriptionHelper;
import com.example.freew.helpers.FragmentSwitchHelper;
import com.example.freew.helpers.ServiceListener;
import com.example.freew.misc.Constants;
import com.example.freew.utils.MyLocationTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.android.libraries.places.api.model.LocationBias;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.example.freew.R2.id.headerBackButtonView;


public class MapFragment extends Fragment implements OnMapReadyCallback,GoogleApiClient.OnConnectionFailedListener{

    private static final String key = "AIzaSyBnsKEfZp23iFteOp4zMukA24NeZVsO03g";

    private AutoCompleteTextView mSearchText;
    private ImageView  mGps;
    private TextView city;
    private TextView apply;
    private ImageView headerBackButtonView;


    private boolean mLocationPermissionGranted = false;
    private static final String TAG = "MapActivity";
    private SupportMapFragment mapFragment;
    private double latitude;
    private double longitude;
    private MyLocationTracker locationTracker;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISION_REQUEST_CODE = 1234;
    private GoogleMap mMap;
    private FusedLocationProviderClient mfusedLocationProviderClient;
    private static final float DEFAULT_ZOOM =15f;
    private PlaceAutocompleteAdapter mPlaceAutocompleteAdapter;
    protected GoogleApiClient mGoogleApiClient;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-40,-168),new LatLng(71,136));
    private SharedPreferences sharedPreferences;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =inflater.inflate(R.layout.fragment_map, container, false);

        mSearchText = view.findViewById(R.id.input_search);
        mGps = view.findViewById(R.id.ic_gps);
        city = view.findViewById(R.id.city);
        headerBackButtonView = view.findViewById(R.id.headerBackButtonView);
        apply = view.findViewById(R.id.apply);
        sharedPreferences = getActivity().getSharedPreferences(Constants.preference, Context.MODE_PRIVATE);
  /*      getLocationPermission();
        if(!sharedPreferences.contains("lastLocation")) {
            getLatLong();
        }else{
            longitude = (double)sharedPreferences.getFloat("lastLocationLatitude",0);
            latitude = (double)sharedPreferences.getFloat("lastLocationLongitude",0);
            //drawMap((double)sharedPreferences.getFloat("lastLocationLatitude",0),(double)sharedPreferences.getFloat("lastLocationLongitude",0));
        }*/

        mSearchText.setOnClickListener(l->mSearchText.setText(""));

        headerBackButtonView.setOnClickListener( l -> {
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(getContext(),new FilterFragment());
        });

        apply.setOnClickListener( l -> {
            sharedPreferences.edit().putBoolean("lastLocation",true).apply();

            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(getContext(),new FilterFragment());
        });

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getLocationPermission();
        if(!sharedPreferences.contains("lastLocation")) {
            getLatLong();
        }else{
            longitude = (double)sharedPreferences.getFloat("lastLocationLatitude",0);
            latitude = (double)sharedPreferences.getFloat("lastLocationLongitude",0);
            //drawMap((double)sharedPreferences.getFloat("lastLocationLatitude",0),(double)sharedPreferences.getFloat("lastLocationLongitude",0));
        }

    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mLocationPermissionGranted) {
            getDeviceLocation();
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.setMinZoomPreference(11);
            mMap.setMaxZoomPreference(18);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            //initMap();
        }


        if(latitude > 0 && longitude > 0){
            Log.d(TAG, "draw method called ");
            drawMap(latitude,longitude);
        }
    }




    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void drawMap(double latitude, double longitude) {
        Log.d("drawMap latitude :",""+latitude);
        Log.d("drawMap longitude :",""+longitude);

        LatLng latLng = new LatLng(latitude, longitude);
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptionHelper.bitmapDescriptorFromVector(getActivity(), R.drawable.ic_map_marker))
                    .title("Marker"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));


            //Initialize Google Play Services
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(getContext(),
                                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(true);
            } else {
                mMap.setMyLocationEnabled(true);
            }
    }


    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mGoogleApiClient = new GoogleApiClient
                .Builder(getContext())
                .addApi(com.google.android.gms.location.places.Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(getActivity(),this)
                .build();
        mPlaceAutocompleteAdapter = new PlaceAutocompleteAdapter(getContext(),mGoogleApiClient,
                LAT_LNG_BOUNDS,null);
        mSearchText.setAdapter(mPlaceAutocompleteAdapter);

        mSearchText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CharSequence charSequence = mPlaceAutocompleteAdapter.getItem(i).getFullText(null);
                //Toast.makeText(MapActivity.this, charSequence, Toast.LENGTH_SHORT).show();
                geoLocate();
            }
        });
        mSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == keyEvent.ACTION_DOWN
                        || keyEvent.getAction() ==keyEvent.KEYCODE_ENTER){
                    // Execute our method for searching
                    geoLocate();
                }
                return false;
            }
        });
        // click on icon of gps
        mGps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: gps icon ");
                getDeviceLocation();
            }
        });
        hideSoftKeyboard();
    }




    private void  getLatLong() {
        locationTracker = new MyLocationTracker(getContext());
        locationTracker.setRequestProcessedListener(new ServiceListener<LatLng>() {
            @Override
            public void result(LatLng result) {
                if (result != null) {
                    longitude = locationTracker.getLongitude();
                    latitude = locationTracker.getLatitude();
                    Log.d("getLatLong latitude :", "" + latitude);
                    Log.d("getLatLong longitude :", "" + longitude);
                    mMap.clear();
                } else {
                    locationTracker.showSettingsAlert();
                }
            }
        });
        if (locationTracker.canGetLocation()) {
            longitude = locationTracker.getLongitude();
            latitude = locationTracker.getLatitude();
            // start service
            //startService(this);
        } else {
            locationTracker.showSettingsAlert();
        }
    }

        private void getDeviceLocation() {
            Log.d(TAG, "getDeviceLocation: getting the device current location ");
            mfusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
            try {
                if (mLocationPermissionGranted) {
                    if (ActivityCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    Task location = mfusedLocationProviderClient.getLastLocation();
                    location.addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                Log.d(TAG, "onComplete: found location!");
                                Location currentLocation = (Location)task.getResult();

                                assert currentLocation != null;


                                Log.d(TAG, String.valueOf(currentLocation.getLatitude() + " " + currentLocation.getLongitude()));
                                //TUT
                                Locale myLocale = new Locale("ru","RU");
                                Geocoder geocoder = new Geocoder(getContext(),myLocale);
                                List<Address> list = new ArrayList<>();

                                try {
                                    list = geocoder.getFromLocation(currentLocation.getLatitude(), currentLocation.getLongitude(), 1);
                                }catch (IOException e){
                                    e.printStackTrace();
                                }
                                String address = list.get(0).getAddressLine(0);
                                String myCity = list.get(0).getLocality();
                                String street = list.get(0).getThoroughfare();
                                String subStreet = list.get(0).getSubThoroughfare();

                                if(street.contains("улица"))
                                    street = street.replace("улица","");

                                city.setText(myCity);
                                mSearchText.setText("ул. " + street + ", д. " + subStreet);


                                sharedPreferences.edit()
                                        .putString("lastLocationCity", myCity)
                                        .putString("lastLocationStreet", "ул. " + street + ", д. " + subStreet)
                                        .putFloat("lastLocationLatitude",(float)currentLocation.getLatitude())
                                        .putFloat("lastLocationLongitude",(float)currentLocation.getLongitude())
                                        .apply();



                                //Toast.makeText(getContext(), address + " " + myCity + " " + street + " " + premise,Toast.LENGTH_SHORT).show();


                                moveCamera(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()),DEFAULT_ZOOM,
                                        "My Location");
                            }else {
                                Log.d(TAG, "onComplete: current location is null");
                                Toast.makeText(getContext(),"Unable to get Locationn ",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }catch (SecurityException e ){
                Log.d(TAG, "getDeviceLocation: Security Exception "+ e.getMessage());  // e.getPrintStack
            }
        }



    private void geoLocate() {
        Log.d(TAG, "geoLocate: geoLocating");
        String searchString = mSearchText.getText().toString();
        Geocoder geocoder = new Geocoder(getContext());
        List<Address> list = new ArrayList<>();
        try{
            list = geocoder.getFromLocationName(searchString,10);
        }catch (IOException e)
        {
            Log.d(TAG, "geoLocate: IOExcepion :" + e.getMessage());
        }
        if (list.size()>0){
            Address address = list.get(0);
            Log.d(TAG, "geoLocate: found a Location" + address.toString());
            moveCamera(new LatLng(address.getLatitude(),address.getLongitude()),DEFAULT_ZOOM,
                    address.getAddressLine(0));
        }
    }

    // camera zoom code
    private void moveCamera(LatLng latLng,float zoom,String title){
        Log.d(TAG, "moveCamera: moving the camera to: lat:" + latLng.latitude + ",lng :" +latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,zoom));
       //if (!title.equals("My Location")) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptionHelper.bitmapDescriptorFromVector(getActivity(), R.drawable.ic_map_marker))
                    .title("Marker"));
       // }
        hideSoftKeyboard();
    }


    private void getLocationPermission() {
        Log.d(TAG, "getlocationPermission: getting location permission");
        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (ContextCompat.checkSelfPermission(getContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        permission, LOCATION_PERMISION_REQUEST_CODE);
            }
        }else {
            ActivityCompat.requestPermissions(getActivity(),
                    permission, LOCATION_PERMISION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called");
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case LOCATION_PERMISION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permision failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionGranted = true;
                    initMap();
                }
            }
        }
    }
    private  void hideSoftKeyboard() {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(locationTracker!=null) {
            locationTracker.stopListener();
        }
        //mMap.clear();
    }


}
