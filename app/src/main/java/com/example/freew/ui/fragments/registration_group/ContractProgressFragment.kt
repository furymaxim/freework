package com.example.freew.ui.fragments.registration_group


import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle

import androidx.core.view.VelocityTrackerCompat
import androidx.fragment.app.Fragment

import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import android.widget.Toast

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper
import com.google.android.material.bottomsheet.BottomSheetBehavior

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnCheckedChanged
import butterknife.OnClick
import butterknife.OnTouch
import butterknife.Unbinder

import android.view.View.SCROLLBAR_POSITION_DEFAULT
import androidx.constraintlayout.widget.Constraints.TAG
import com.example.freew.misc.Constants.myPreference
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.bottom_sheet_contract.*
import kotlinx.android.synthetic.main.contract_main.*
import kotlinx.android.synthetic.main.header_common.*


class ContractProgressFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.fragment_contract_progress

    private var sharedPreferences: SharedPreferences? = null
    private var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    private var mTracker:VelocityTracker? = null
    private var lastKnownYVelocity = 0
    private var runnable:Runnable? =null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        sharedPreferences = activity!!.getSharedPreferences(myPreference, Context.MODE_PRIVATE)
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetContract!!)
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN


        isCheckboxChecked()


        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
        }

        cbAgree.setOnCheckedChangeListener { _, isChecked ->

            if (isChecked) {
                saveContract.setTextColor(resources.getColor(R.color.colorBlue_900))
                saveContract.setOnClickListener {
                    sharedPreferences!!.edit().putBoolean("contractCheckSuccess", true).apply()
                    FragmentSwitchHelper.switchFragmentWithoutBackStack(context, ContractCopyFragment())
                }
            } else {
                saveContract.setTextColor(resources.getColor(R.color.colorBlue_500))
                saveContract.setOnClickListener(null)
            }

        }

        scrollView.setOnTouchListener { _, event ->

            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN

            when(event.action){
                MotionEvent.ACTION_DOWN -> {
                    if (mTracker == null) {
                        mTracker = VelocityTracker.obtain()
                    } else {
                        mTracker!!.clear()
                    }
                    mTracker!!.addMovement(event)
                }

                MotionEvent.ACTION_UP->{
                    if(lastKnownYVelocity < - 850) {
                        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
                        runnable=Runnable{
                            scrollView.fullScroll(ScrollView.FOCUS_UP)
                        }
                        scrollView.post(runnable)
                    }
                }
                MotionEvent.ACTION_MOVE->{
                    mTracker!!.addMovement(event)
                    mTracker!!.computeCurrentVelocity(1000)

                    lastKnownYVelocity = mTracker!!.yVelocity.toInt()
                    bottomSheetBehavior!!.setState(BottomSheetBehavior.STATE_HIDDEN)

                }
                MotionEvent.ACTION_CANCEL->{
                    mTracker!!.recycle()
                }

            }

            false
        }

    }

    private fun isCheckboxChecked(){
        if (!cbAgree.isChecked) {
            saveContract.setOnClickListener(null)
        }
    }

}




