package com.example.freew.ui.base

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.header_common.*

abstract class TitleFragment : BaseFragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        headerBackButtonView.setOnClickListener {
            if (fragmentManager?.backStackEntryCount!! > 0)
                fragmentManager?.popBackStack()
        }
    }
}
