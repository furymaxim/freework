package com.example.freew.ui.fragments.index


import android.Manifest
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import com.example.freew.R

import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.misc.Constants
import com.example.freew.ui.activities.AppActivity
import com.example.freew.ui.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_filter.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.header_common.*


class FilterFragment : Fragment() {

    private var sharedPreferences: SharedPreferences? = null
    private val FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION
    private val COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
    private val LOCATION_PERMISION_REQUEST_CODE = 1234

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        sharedPreferences = activity!!.getSharedPreferences(Constants.preference, Context.MODE_PRIVATE)
        val activity = getActivity() as AppActivity

        activity.hideBottomNavigationView()

        val step = 5
        val max = 150
        val min = 5
        seekBar.max = ((max - min) / step)

        getLocationPermission()

        doesContainLastLocation()


        if(sharedPreferences!!.getBoolean("fromMain",false)){

            doesContainFinalSortBy()
            doesContainFinalSortByCompanies()
            doesContainFinalSortByCategories()


            seekBar.progress = sharedPreferences!!.getInt("filterTask",5)
            currentDistance.text = (min + seekBar.progress * 5).toString()
            sortPayoutRate.setText(sharedPreferences!!.getInt("payoutRate",1200).toString())
            switcher.isChecked = sharedPreferences!!.getBoolean("switcher",false)

        }else{

            doesContainSortBy()
            doesContainSortByCompanies()
            doesContainSortByCategories()

            seekBar.progress = sharedPreferences!!.getInt("tempFilterTask",5)
            currentDistance.text = (min + seekBar.progress * 5).toString()
            sortPayoutRate.setText(sharedPreferences!!.getInt("tempPayoutRate",1200).toString())
            switcher.isChecked = sharedPreferences!!.getBoolean("tempSwitcher",false)
        }


        sortBy.setOnClickListener {
            putTempData()
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, SortByFragment())
        }

        sortCompany.setOnClickListener {
            putTempData()
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, SortCompaniesFragment())
        }

        sortCategory.setOnClickListener {
            putTempData()
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, SortCategoriesFragment())
        }


        apply.setOnClickListener {
            if(sortPayoutRate.text.isEmpty()){
                Toast.makeText(context,"Значение должно быть от 300 до 2500", Toast.LENGTH_SHORT).show()
            }else if(sortPayoutRate.text.toString().toInt() < 300 || sortPayoutRate.text.toString().toInt() > 2500) {
                    sortPayoutRate.setTextColor(resources.getColor(R.color.colorRed_900))
                    Toast.makeText(context,"Значение должно быть от 300 до 2500", Toast.LENGTH_SHORT).show()
            }else{
                sharedPreferences!!.edit().putInt("filterTask", seekBar.progress).apply()
                sharedPreferences!!.edit().putInt("finalSortBy", sharedPreferences!!.getInt("sortBy", 1)).apply()
                sharedPreferences!!.edit().putInt("finalSortByCompanies",sharedPreferences!!.getInt("sortByCompanies",0)).apply()
                sharedPreferences!!.edit().putInt("finalSortByCategories",sharedPreferences!!.getInt("sortByCategories",0)).apply()
                sharedPreferences!!.edit().putInt("payoutRate", sortPayoutRate.text.toString().toInt()).apply()
                sharedPreferences!!.edit().putBoolean("switcher",switcher.isChecked).apply()
                FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, HomeFragment())
            }
        }


        headerBackButtonView.setOnClickListener{
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, HomeFragment())
        }

        map.setOnClickListener {
            FragmentSwitchHelper.switchFragmentMainWithoutBackStack(context, MapFragment())
        }

        sortPayoutRate.setOnClickListener {
            sortPayoutRate.setTextColor(resources.getColor(R.color.colorBlue_900))
        }


        sortPayoutRate.addTextChangedListener(object: TextWatcher{

            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                sortPayoutRate.setTextColor(resources.getColor(R.color.colorBlue_900))
            }
        })



        seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val value = min + (progress * step)
                currentDistance.text = (value.toString())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })

    }


    private fun getLocationPermission() {
        val permission = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (ContextCompat.checkSelfPermission(context!!,
                        FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(context!!, COURSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity!!,
                        permission, LOCATION_PERMISION_REQUEST_CODE)
            }
        } else {
            ActivityCompat.requestPermissions(activity!!,
                    permission, LOCATION_PERMISION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()) {
                    for (i in grantResults.indices) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            return
                        }
                    }
                }
            }
        }
    }


    private fun doesContainLastLocation(){
        if(!sharedPreferences!!.contains("lastLocation")){
            detailTitle.text = ("Адрес не указан")
        }else{
            val currentCity = sharedPreferences!!.getString("lastLocationCity","")
            val currentStreet = sharedPreferences!!.getString("lastLocationStreet","")
            detailTitle.text = "$currentCity\n$currentStreet"
        }
    }

    private fun doesContainFinalSortBy(){
        if(!sharedPreferences!!.contains("finalSortBy")){
            sortBy.text = "Все"
        }else{
            if(sharedPreferences!!.getInt("finalSortBy",0) == 1){
                sortBy.text = "Все"
            }

            if(sharedPreferences!!.getInt("finalSortBy",0) == 2){
                sortBy.text = "Убыванию оплаты"
            }

            if(sharedPreferences!!.getInt("finalSortBy",0) == 3){
                sortBy.text = "Удаленности"
            }

            if(sharedPreferences!!.getInt("finalSortBy",0) == 4){
                sortBy.text = "Рейтингу компаний"
            }
        }
    }

    private fun doesContainFinalSortByCompanies(){
        if(!sharedPreferences!!.contains("finalSortByCompanies")){
            sortCompany.text = "Все"
        }else{
            if(sharedPreferences!!.getInt("finalSortByCompanies",0) == 0){
                sortCompany.text = "Все"
            }

            if(sharedPreferences!!.getInt("finalSortByCompanies",0) == 1){
                sortCompany.text = "1 компания"
            }

            if(sharedPreferences!!.getInt("finalSortByCompanies",0)  > 1){
                sortCompany.text = (sharedPreferences!!.getInt("finalSortByCompanies",0).toString() + " компаний")
            }
        }
    }

    private fun doesContainFinalSortByCategories(){
        if(!sharedPreferences!!.contains("finalSortByCategories")){
            sortCategory.text = "Все"
        }else{
            if(sharedPreferences!!.getInt("finalSortByCategories",0) == 0){
                sortCategory.text = "Все"
            }

            if(sharedPreferences!!.getInt("finalSortByCategories",0) == 1){
                sortCategory.text = "1 категория"
            }

            if(sharedPreferences!!.getInt("finalSortByCategories",0)  > 1){
                sortCategory.text = (sharedPreferences!!.getInt("finalSortByCategories",0).toString() + " категорий")
            }

        }
    }

    private fun doesContainSortBy(){
        if(!sharedPreferences!!.contains("sortBy")){
            sortBy.text = "Все"
        }else{
            if(sharedPreferences!!.getInt("sortBy",0) == 1){
                sortBy.text = "Все"
            }

            if(sharedPreferences!!.getInt("sortBy",0) == 2){
                sortBy.text = "Убыванию оплаты"
            }

            if(sharedPreferences!!.getInt("sortBy",0) == 3){
                sortBy.text = "Удаленности"
            }

            if(sharedPreferences!!.getInt("sortBy",0) == 4){
                sortBy.text = "Рейтингу компаний"
            }
        }
    }

    private fun doesContainSortByCompanies(){
        if(!sharedPreferences!!.contains("sortByCompanies")){
            sortCompany.text = "Все"
        }else{
            if(sharedPreferences!!.getInt("sortByCompanies",0) == 0){
                sortCompany.text = "Все"
            }

            if(sharedPreferences!!.getInt("sortByCompanies",0) == 1){
                sortCompany.text = "1 компания"
            }

            if(sharedPreferences!!.getInt("sortByCompanies",0)  > 1){
                sortCompany.text = (sharedPreferences!!.getInt("sortByCompanies",0).toString() + " компаний")
            }
        }
    }

    private fun doesContainSortByCategories(){
        if(!sharedPreferences!!.contains("sortByCategories")){
            sortCategory.text = "Все"
        }else{
            if(sharedPreferences!!.getInt("sortByCategories",0) == 0){
                sortCategory.text = "Все"
            }

            if(sharedPreferences!!.getInt("sortByCategories",0) == 1){
                sortCategory.text = "1 категория"
            }

            if(sharedPreferences!!.getInt("sortByCategories",0)  > 1){
                sortCategory.text = (sharedPreferences!!.getInt("sortByCategories",0).toString() + " категорий")
            }
        }
    }

    private fun putTempData(){
        sharedPreferences!!.edit().putBoolean("fromMain", false).apply()
        sharedPreferences!!.edit().putInt("tempFilterTask", seekBar.progress).apply()
        sharedPreferences!!.edit().putInt("tempPayoutRate", sortPayoutRate.text.toString().toInt()).apply()
        sharedPreferences!!.edit().putBoolean("tempSwitcher",switcher.isChecked).apply()
    }


    companion object {
        fun newInstance(): FilterFragment {
            val args = Bundle()
            val fragment = FilterFragment()
            fragment.arguments = args
            return fragment
        }
    }


}
