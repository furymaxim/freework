package com.example.freew.ui.viewmodel

import com.example.freew.api.PhoneResponse
import com.example.freew.api.SessionResponse
import com.example.freew.api.WebApi
import com.example.freew.helpers.DateHelper
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class RegistrationViewModel : BaseViewModel() {
    val inn: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isInnValid: Observable<Boolean> = inn.map { it.isNotBlank() && it.length == 12  }

    val snils: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isSnilsValid: Observable<Boolean> = snils.map { it.isNotBlank() && it.length == 11  }

    val email: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isEmailValid: Observable<Boolean> = email.map { it.isNotBlank() && isValidEmailAddress(it)  }

    val regAddress:BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isRegAddressValid: Observable<Boolean> = snils.map { it.isNotBlank() && it.length > 10  }

    val lastName: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isLastNameValid: Observable<Boolean> = lastName.map { it.isNotBlank() && it.length > 3  }

    val firstName: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isFirstNameValid: Observable<Boolean> = firstName.map { it.isNotBlank() && it.length > 2  }

    val patrynomic: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isPatrynomicValid: Observable<Boolean> = patrynomic.map { it.isNotBlank() && it.length > 5  }

    val birthDate: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isBirthDateValid: Observable<Boolean> = birthDate.map { it.isNotEmpty() && DateHelper.isValidBirthday(it) && it.length == 10}

    val sex: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isSexValid: Observable<Boolean> = sex.map { it.isNotBlank() && (it.indexOf("м") > -1 || it.indexOf("ж") > -1)}

    val address: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isAddressValid: Observable<Boolean> = address.map { it.isNotBlank() && it.length > 15}

    val pasOrg: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isPasOrgValid: Observable<Boolean> = pasOrg.map { it.isNotBlank() && it.length > 10}

    val pasNumberSeries: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isPasNumberSeriesValid: Observable<Boolean> = pasNumberSeries.map { it.isNotBlank() && it.length > 11}

    val pasCode: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isPasCodeValid: Observable<Boolean> = pasNumberSeries.map { it.isNotBlank() && it.length > 6}

    val pasDate: BehaviorRelay<String> = BehaviorRelay.createDefault("")
    val isPasDateValid: Observable<Boolean> = pasDate.map { it.isNotEmpty() && DateHelper.isValidBirthday(it) && it.length == 10}


    private fun isValidEmailAddress(email: String): Boolean {
        val ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$"
        val p = java.util.regex.Pattern.compile(ePattern)
        val m = p.matcher(email)
        return m.matches()
    }


}