package com.example.freew.ui.fragments.registration_group


import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log

import androidx.fragment.app.Fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.ui.base.BaseFragment

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnCheckedChanged
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.extensions.bindClicks

import com.example.freew.misc.Constants.myPreference
import com.example.freew.ui.viewmodel.RegistrationViewModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.jakewharton.rxbinding2.widget.RxTextView
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_inn_progress.*
import kotlinx.android.synthetic.main.header_common.*
import org.reactivestreams.Subscription
import java.util.*


class InnProgressFragment : BaseFragment(){


    private val model = RegistrationViewModel()

    override val layoutId: Int
        get() = R.layout.fragment_inn_progress

    private var sharedPreferences: SharedPreferences? = null


    private var innCorrect = false
    private var snilsCorrect = false
    private var addressCorrect = false
    private var emailCorrect = false


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        sharedPreferences = activity!!.getSharedPreferences(myPreference, Context.MODE_PRIVATE)


        val dis1 = editTextInnNumber.textChanges().map { it.toString()}.subscribe { model.inn.accept(it) }
        val dis2 = editTextAddress.textChanges().map { it.toString()}.subscribe { model.regAddress.accept(it) }
        val dis3 = editTextEmail.textChanges().map { it.toString()}.subscribe { model.email.accept(it) }
        val dis4 = editTextSnilsNumber.textChanges().map{it.toString()}.subscribe{ model.snils.accept(it) }


        innHelp.setOnClickListener {

            val newFragment: DialogFragment?= InnHelpFragment()

            newFragment?.show(fragmentManager!!,"dialog")
        }

        switch1.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                snilsNumber.visibility = View.VISIBLE
                snilsEdit.visibility = View.VISIBLE
                divSnilsNumber.visibility = View.VISIBLE
                deleteSnils.visibility = View.INVISIBLE
                editTextSnilsNumber.setText("")
                snilsCorrect = false
                saveInn.setTextColor(resources.getColor(R.color.colorBlue_500))
                saveInn.setOnClickListener(null)

            } else {
                snilsNumber.visibility = View.GONE
                snilsEdit.visibility = View.GONE
                divSnilsNumber.visibility = View.GONE
                deleteSnils.visibility = View.INVISIBLE
                snilsCorrect = true

                if (innCorrect && snilsCorrect && addressCorrect && emailCorrect) {
                    saveInn.setTextColor(resources.getColor(R.color.colorBlue_900))
                    saveInn.setOnClickListener {
                        sharedPreferences!!.edit().putBoolean("innCheckSuccess", true).apply()
                        FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
                    }

                }

                if (!switch1.isChecked) {
                    snilsCorrect = true
                }
            }
        }

        editTextInnNumber.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteInnNumber.visibility = View.VISIBLE

                }else{
                    deleteInnNumber.visibility = View.GONE
                }


                if(!switch1.isChecked)
                    snilsCorrect = true

                if(s.length >11) innCorrect = true else innCorrect = false


                isAllSuccess()

            }
        })



        editTextSnilsNumber.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if(s!!.isNotBlank()){
                    deleteSnils.visibility = View.VISIBLE

                }else{
                    deleteSnils.visibility = View.GONE
                }


                if(!switch1.isChecked)
                    snilsCorrect = true


                if(s.length > 10) snilsCorrect = true else snilsCorrect = false

                isAllSuccess()

            }
        })


        editTextEmail.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteEmail.visibility = View.VISIBLE

                }else{
                    deleteEmail.visibility = View.GONE
                }


                if(!switch1.isChecked)
                    snilsCorrect = true



                if(s.indexOf("@") > 0 && s.indexOf(".")  > 0 && s.length > 6) emailCorrect = true else emailCorrect= false

                isAllSuccess()

            }
        })




        editTextAddress.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.isNotBlank()){
                    deleteAddress.visibility = View.VISIBLE

                }else{
                    deleteAddress.visibility = View.GONE
                }


                if(!switch1.isChecked)
                    snilsCorrect = true

                if(s.length > 10) addressCorrect= true else addressCorrect = false

                isAllSuccess()

            }
        })

        deleteSnils.setOnClickListener{
            editTextSnilsNumber.setText("")
            snilsCorrect = false
            deleteSnils.visibility = View.GONE
            setSaveInnFail()
        }

        deleteInnNumber.setOnClickListener {
            editTextInnNumber.setText("")
            innCorrect = false
            deleteInnNumber.visibility = View.GONE
            setSaveInnFail()
        }

        deleteEmail.setOnClickListener {
            editTextEmail.setText("")
            emailCorrect = false
            deleteEmail.visibility = View.GONE
            setSaveInnFail()
        }

        deleteAddress.setOnClickListener {
            editTextAddress.setText("")
            addressCorrect = false
            deleteAddress.visibility = View.GONE
            setSaveInnFail()
        }

        headerBackButtonView.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
        }


    }

    private fun isAllSuccess(){
        if(innCorrect && snilsCorrect && addressCorrect && emailCorrect){
            saveInn.setTextColor(resources.getColor(R.color.colorBlue_900))
            saveInn.setOnClickListener{
                sharedPreferences!!.edit().putBoolean("innCheckSuccess",true).apply()
                FragmentSwitchHelper.switchFragmentWithoutBackStack(context, RegistrationStepsFragment.newInstance())
            }
        }else{
            setSaveInnFail()
        }
    }

    private fun setSaveInnFail(){
        saveInn.setTextColor(resources.getColor(R.color.colorBlue_500))
        saveInn.setOnClickListener(null)
    }

}




