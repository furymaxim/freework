package com.example.freew.ui.activities

import android.os.Bundle
import com.example.freew.R
import com.example.freew.ui.base.BaseActivity
import com.example.freew.ui.fragments.index.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_app.*


class AppActivity : BaseActivity() {


    override val layoutId = R.layout.activity_app

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setDefaultItem()

    }

    private val navListener = object: BottomNavigationView.OnNavigationItemSelectedListener{
        override fun onNavigationItemSelected(@NonNull item: MenuItem): Boolean {
            var selectedFragment: Fragment? = null

            when (item.itemId) {
                R.id.navHome -> {
                    selectedFragment = HomeFragment.newInstance()
                    bottomNavigation.menu.getItem(0).setIcon(R.drawable.ic_home_selected)
                }

            }

            supportFragmentManager.beginTransaction().replace(R.id.fragment,
                    selectedFragment!!).commit()

            return true
        }
    }

    private fun setDefaultItem(){
        bottomNavigation.setOnNavigationItemSelectedListener(navListener)
        bottomNavigation.selectedItemId = R.id.navHome
        bottomNavigation.menu.getItem(0).setIcon(R.drawable.ic_home_selected)


    }

    override fun onBackPressed() {
        //super.onBackPressed()
    }


    fun hideBottomNavigationView(){
        bottomNavigation.visibility = View.GONE
    }

    fun showBottomNavigationView(){
        bottomNavigation.visibility = View.VISIBLE
    }
}
