package com.example.freew.ui.fragments.login_group

import android.os.Bundle

import androidx.fragment.app.Fragment

import android.os.CountDownTimer
import android.os.Handler
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText

import com.example.freew.R

import butterknife.ButterKnife
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog
import com.example.freew.api.SessionResponse

import com.example.freew.extensions.bindClicks
import com.example.freew.extensions.triggerLoadingIndicator
import com.example.freew.helpers.FragmentSwitchHelper
import com.example.freew.helpers.UiHelpers
import com.example.freew.misc.Parameters
import com.example.freew.ui.base.LoaderShower
import com.example.freew.ui.base.TitleFragment
import com.example.freew.ui.viewmodel.LoginViewModel
import com.example.freew.ui.widget.PinEntryEditText
import com.jakewharton.rxbinding2.widget.textChanges
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import kotlinx.android.synthetic.main.fragment_phone_confirmation.*
import kotlinx.android.synthetic.main.header_common.*
import java.text.DecimalFormat


class PhoneConfirmationFragment : TitleFragment(), LoaderShower, Runnable {

    override val layoutId = R.layout.fragment_phone_confirmation
    private val model = LoginViewModel()
    private val handler = Handler()
    private val formatter = DecimalFormat("00")
    private val smsTimeoutSeconds = 30
    private var seconds = 0
    private lateinit var loadingIndicator: MaterialDialog


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setupEditText(codeView)
        setupKeyboardForInput(codeView)
        loadingIndicator = UiHelpers.createProgressDialog(context)
        model.phone.accept(arguments!!.getString(Parameters.PHONE)!!)
        val sendSmsRequest = model.smsCodeRequest.triggerLoadingIndicator(this).doOnSubscribe { startTimer() }
        val dis1 = codeView.textChanges().map { it.toString() }.subscribe{model.phone.accept(it)}
        val dis2 = bindRequest(sendSmsRequest).subscribe({}, {})
        val dis3 = resendView.bindClicks(this, this, sendSmsRequest).subscribe({}, {})
        codeView.setOnPinEnteredListener { bindRequest(model.loginRequest.triggerLoadingIndicator(this)).subscribe({ goNext(it) }, { showIncorrectText()}) }

        headerBackButtonView.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, AppFirstV2Fragment.newInstance())
        }
    }

    private fun showIncorrectText(){
        resendView.text = "Неверный код"
        resendView.setTextColor(resources.getColor(R.color.colorRed_900))
    }


    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(this)
    }

    override fun onStart() {
        super.onStart()
        if (seconds > 0) {
            handler.post(this)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(Parameters.SECONDS, seconds)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) {
            this.seconds = savedInstanceState.getInt(Parameters.SECONDS, 0)
        }
    }

    override fun showLoader() {
        if (!loadingIndicator.isShowing) {
            loadingIndicator.show()
        }
    }

    override fun hideLoader() {
        if (loadingIndicator.isShowing) {
            loadingIndicator.dismiss()
        }
    }

    override fun run() {
        seconds -= 1
        if (seconds == 0) {
            resendView.isEnabled = true
            resendView.visibility = View.VISIBLE
            resendView.setText(R.string.resend_sms)
            handler.removeCallbacks(this)
        } else if (seconds > 0) {
            resendView.isEnabled = false
            resendView.visibility = View.VISIBLE
            resendView.text = getString(R.string.resend_sms_after, formatter.format(seconds.toLong()))
            handler.postDelayed(this, 1000)
        } else {
            resendView.isEnabled = false
            resendView.visibility = View.INVISIBLE
        }
    }

    private fun startTimer() {
        seconds = smsTimeoutSeconds
        handler.post(this)
    }

    private fun goNext(session: SessionResponse) {
        val phone = arguments!!.getString(Parameters.PHONE)
        val fragment = BirthDateSetupFragment.newInstance(phone)
        navigateNext(fragment)
    }

    private fun setupKeyboardForInput(editText: PinEntryEditText) {
        val ic = editText.onCreateInputConnection(EditorInfo())
        keyboardInput?.setInputConnection(ic)
    }


    companion object {
        fun newInstance(phone: String): PhoneConfirmationFragment {
            val args = Bundle()
            args.putString(Parameters.PHONE, phone)
            val fragment = PhoneConfirmationFragment()
            fragment.arguments = args
            return fragment
        }

    }


    private fun setupEditText(editText: PinEntryEditText) {
        editText.isFocusable = false
        editText.isClickable = false
    }


}
