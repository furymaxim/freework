package com.example.freew.ui.base

interface ErrorShower {

    fun showError(error: Throwable)
}