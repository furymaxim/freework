package com.example.freew.ui.viewmodel

import com.example.freew.api.BaseResponse
import com.example.freew.misc.Constants

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
open class BaseViewModel {
    protected fun <T : BaseResponse> standardRequest(observable: Observable<T>): Observable<T> {
        val timeOut = Constants.TIMEOUT_IN_SECONDS

        return observable
            .timeout(timeOut, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
