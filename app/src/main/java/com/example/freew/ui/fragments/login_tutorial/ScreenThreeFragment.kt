package com.example.freew.ui.fragments.login_tutorial


import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.freew.R
import com.example.freew.R2
import com.example.freew.helpers.FragmentSwitchHelper

import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.example.freew.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_screen_one.*


class ScreenThreeFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.fragment_screen_three


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goNext.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, ScreenFourFragment.newInstance())
        }
    }

    companion object {
        fun newInstance(): ScreenThreeFragment {
            val args = Bundle()
            val fragment = ScreenThreeFragment()
            fragment.arguments = args
            return fragment
        }
    }

}
