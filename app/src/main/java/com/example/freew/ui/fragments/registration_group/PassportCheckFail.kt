package com.example.freew.ui.fragments.registration_group


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.freew.R
import com.example.freew.helpers.FragmentSwitchHelper
import kotlinx.android.synthetic.main.fragment_passport_check_fail.*

class PassportCheckFail : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_passport_check_fail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goToCamera.setOnClickListener {
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, CameraFragment())
        }

    }
}



