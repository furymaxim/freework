package com.example.freew.ui.fragments.login_group

import android.os.Bundle

import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText

import com.example.freew.R

import com.example.freew.helpers.FragmentSwitchHelper

import com.example.freew.api.BaseResponse
import com.example.freew.extensions.bindClicks
import com.example.freew.extensions.bindClicksWithErrors
import com.example.freew.helpers.DateHelper
import com.example.freew.helpers.KeyboardHelper
import com.example.freew.helpers.UiHelpers
import com.example.freew.misc.Parameters
import com.example.freew.ui.base.TitleFragment
import com.example.freew.ui.fragments.login_tutorial.ScreenOneFragment
import com.example.freew.ui.viewmodel.LoginViewModel
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.textChanges
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.internal.util.NotificationLite.accept
import kotlinx.android.synthetic.main.fragment_birth_date_setup.*
import kotlinx.android.synthetic.main.fragment_birth_date_setup.nextView
import kotlinx.android.synthetic.main.fragment_phone_input.*
import kotlinx.android.synthetic.main.header_common.*
import org.reactivestreams.Subscription
import java.util.*

class BirthDateSetupFragment :  TitleFragment(){

    override val layoutId = R.layout.fragment_birth_date_setup
    private val model = LoginViewModel()




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)




        val dis1 = birthInput.textChanges().map { it.toString() }.subscribe { model.birthday.accept(it) }




        val dis2 = nextView.bindClicks(this, this, model.birthday, model.isBirthValid).subscribe({goNext()},{})
        birthInput.setRawInputType(InputType.TYPE_CLASS_NUMBER)
        UiHelpers.installDateMaskOn(birthInput)



        setupKeyboardForInput(birthInput)


        birthInput.addTextChangedListener(object:TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(s!!.length == 10 && DateHelper.isValidBirthday(s.toString())){
                    nextView?.setTextColor(getResources().getColor(R.color.colorBlue_900))
                }else{
                    nextView?.setTextColor(getResources().getColor(R.color.colorBlue_500))
                }
            }
        })

        headerBackButtonView.setOnClickListener{
            FragmentSwitchHelper.switchFragmentWithoutBackStack(context, AppFirstV2Fragment.newInstance())
        }
    }

    private fun goNext() {
        val fragment = ScreenOneFragment.newInstance()
        navigateNext(fragment)
    }

    private fun showFail() {
        val phone = arguments!!.getString(Parameters.PHONE)
        val fragment = LoginFailFragment.newInstance(phone!!)
        navigateNext(fragment)
    }

    private fun setupKeyboardForInput(editText: EditText) {
        val ic = editText.onCreateInputConnection(EditorInfo())
        keyboardBirth.setInputConnection(ic)
    }


    companion object {
        fun newInstance(phone: String): BirthDateSetupFragment {
            val args = Bundle()
            args.putString(Parameters.PHONE, phone)
            val fragment = BirthDateSetupFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
