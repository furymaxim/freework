package com.example.freew.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.LayoutRes
import com.example.freew.extensions.showErrorDialog
import com.trello.rxlifecycle2.android.ActivityEvent
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity
import com.trello.rxlifecycle2.kotlin.bindUntilEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

abstract class BaseActivity : RxAppCompatActivity() {
    @get:LayoutRes
    protected abstract val layoutId: Int

    companion object {
        public fun start(context: Context, clazz: Class<*>) {
            val starter = Intent(context, clazz)
            starter.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
        onViewBound()
        bindToViewModel()
    }

    open fun onViewBound() {}

    open fun bindToViewModel() {}

    protected open fun showError(error: Throwable) {
        this.showErrorDialog(error)
    }

    fun <T> bindRequest(action: Observable<T>): Observable<T> {
        return action.observeOn(AndroidSchedulers.mainThread()).doOnError(::showError)
            .bindUntilEvent(this, ActivityEvent.DESTROY)
    }
}

